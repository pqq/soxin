# soxin

all nixos related…

## status

### tl;dr

everything but auto-mounting works

### overview

learning nixos felt very rewarding. however, then i learned about nix's involvement with the mic, or the military industrial complex (see https://nixos-users-against-mic-sponsorship.github.io/). that led to me deciding to pull the plug on my nixos endavour at the end of april 24, three months after i started looking at this os.

everything works, however, i never sorted out auto mounting:
- https://discourse.nixos.org/t/trying-to-make-a-directory-from-systemd-results-in-a-127-error-code/44058
- https://discourse.nixos.org/t/systemd-user-mounts-is-having-no-effect/44128
- https://discourse.nixos.org/t/is-it-possible-to-run-systemd-units-as-root-via-home-manager/44166

i tried to auto mount in two different ways, and the related config is found at:  
- `dots/hosts/t470p/home.nix`
- `dots/modules/programs/auto-mount/default.nix` (currently disabled via `options.nix`.)

### scripts

#### genesis

for setting up "curae".

- `cd /tmp`
- `wget tinyurl.com/fk-genesis`     # same as `wget https://gitlab.com/pqq/soxin/-/raw/main/scripts/genesis.sh`
- `cat fk-genesis`                  # verify the downloaded script
- `chmod 755 fk-genesis`            # make the script executable
- `./fk-genesis`                    # run the script

#### curae

for building and maintaining the system.

**first build**
- `cd /home/.curae/`
- `./curae.sh urh [host]`
  - same as:
  - `./curae.sh upd-dots all`
  - `./curae.sh rebuildh [host]`

**sequencial builds**
- `ur` (full path and host is not needed after first build)

### other setup

**nexcloud sync**

- start nextcloud and set up sync
  - add url to nextcloud instance
  - local folder: choose different folder ➔ `/home/poq/nextcloud`
  - remote: choose what to sync ➔ syncDir

**ssh keys deployment**
- `~/.local/bin/deploySshKeys.sh`

### tools

- `nmtui` / `nmcli` - `nm-applet` :: network management

### faq

- how to start
  - login as poq, and run the `Hyprland` command

## todos

- remanining issues:
  - https://gitlab.com/pqq/pIssues.2/-/issues/228#note_1859665448
- look into:
  - lf | https://github.com/gokcehan/lf/wiki/Tutorial
  - neovim
  - git
  - tmux | https://hamvocke.com/blog/a-quick-and-easy-guide-to-tmux/

## considerations

**screensharing**

is the following still needed for screensharing?:
```
# screen sharing
services.dbus.enable = true;
xdg.portal = {
    enable = true;
    wlr.enable = true;
    extraPortals = [
        pkgs.xdg-desktop-portal-gtk
    ];
};
```

## programs up for considerations

```
# srm"                           # secure delete
# bc cli calculator
#   `scale=2` for two decimals
#  - https://www.baeldung.com/linux/calculators-cli
# speedcrunch"                   # calculator
# MOVE UP AS NEEDED
# gparted"                       # tool for partitioning and disk maintenance
# gpicview"                      # image viewer
# gufw"                          # gui for the firewall
# inkscape"                      # inkscape vector graphics editor
# libreoffice-fresh"             # ms office replacement
# noto-fonts-cjk"                # asian characters fonts
# mutt"                          # text based email client (used for mbox files)
# net-tools"                     # collection of base networking utilities
# nginx"                         # web server
# obs-studio"                    # open broadcast studio - for screen recording
# onlyoffice"                    # ms office replacement
# qutebrowser"                   # web browser
# seahorse"                      # gui for viewing keyring entries / managing pgp keys
# signal-desktop"                # signal messenger
# tor"                           # the onion router
# xarchiver"                     # gui for cli archiving commands (zip, tar, rar, 7zip etc)
# brave-bin"                     # brave browser
# brother-hll2310d"              # printer (main)
# brother-ql700"                 # printer (label printer)
# brother-ql1100"                # printer (label printer)
# cryptomator-bin"               # encryption tool
# exodus"                        # crypto wallet
# imgp"                          # Multi-core batch image resizer and rotator (https://github.com/jarun/imgp)
# nohang-git"                    # avoid crash/hang due to running out of memory (https://archived.forum.manjaro.org/t/solved-display-warning-message-or-kill-program-before-system-runs-out-of-memory/147635/24)
# pdfarranger-git"               # gui tool to arrange pdf files
# postman-bin"                   # api development tool
# protonmail-import-export-app-bin"
# spideroak-one"                 # backup tool
# synology-assistant"            # help discovering your nas (disable firewall first, via "gufw")
# sublime-text-dev"              # text editor
# vdhcoapp-bin"                  # Companion application for Video DownloadHelper browser add-on
```
