# nixos :: flake and home-manager :: klevstul

{
    description = "nixos :: flake and home-manager :: klevstul";

    # 'inputs' are the dependencies of the flake, and
    # 'outputs' function will return all the build results of the flake.
    # each item in 'inputs' will be passed as a parameter to
    # the `outputs` function after being pulled and built.

    inputs = {

        # There are many ways to reference flake inputs.
        # The most widely used is `github:owner/name/reference`,
        # which represents the GitHub repository URL + branch/commit-id/tag.

        # example of using a specific commit hash:
        # https://discourse.nixos.org/t/cant-update-nvidia-driver-on-stable-branch/39246/17?u=dob

        # official nixos package source
        nixpkgs.url = "github:nixos/nixpkgs/nixos-23.11";

        # unstable nixpkgs
        nixpkgs-unstable.url = "github:nixos/nixpkgs/nixos-unstable";

        # home manager, used for managing user configuration
        # https://nixos.wiki/wiki/Home_Manager
        # https://nix-community.github.io/home-manager/
        home-manager = {
            url = "github:nix-community/home-manager/release-23.11";
            # The `follows` keyword in inputs is used for inheritance.
            # Here, `inputs.nixpkgs` of home-manager is kept consistent with
            # the `inputs.nixpkgs` of the current flake,
            # to avoid problems caused by different versions of nixpkgs.
            inputs.nixpkgs.follows = "nixpkgs";
        };

        # unstable home-manager
        home-manager-unstable = {
            url = "github:nix-community/home-manager";
            inputs.nixpkgs.follows = "nixpkgs-unstable";
        };

    };

    # 'outputs' are all the build result of the flake.
    #
    # A flake can have many use cases and different types of outputs.
    #
    # parameters in function 'outputs' are defined in 'inputs' and
    # can be referenced by their names. However, 'self' is an exception,
    # this special parameter points to the 'outputs' itself(self-reference)
    #
    # The `@` syntax here is used to alias the attribute set of the
    # inputs's parameter, making it convenient to use inside the function.

    outputs = {
        nixpkgs,
        ...
    } @inputs :{

        # UPDATE: Using overlays is overkill. For more details, see:
        # https://discourse.nixos.org/t/unstable-channel-undefined-variable-unstable/42130
        # ---
        # When applied, the unstable nixpkgs set (declared in the flake inputs) will
        # be accessible through 'pkgs.unstable'
        #unstable-packages = final: _prev: {
        #    unstable = import inputs.nixpkgs-unstable {
        #        system = final.system;
        #        config.allowUnfree = true;
        #    };
        #};

        # NixOS configuration entrypoint
        # Available through 'nixos-rebuild --flake .#your-hostname'

        nixosConfigurations = {

            # By default, NixOS will try to refer the nixosConfiguration with
            # its hostname, so the system named `t470p` will use this one.
            # However, the configuration name can also be specified using:
            #   sudo nixos-rebuild switch --flake /path/to/flakes/directory#<name>
            #
            # The `nixpkgs.lib.nixosSystem` function is used to build this
            # configuration, the following attribute set is its parameter.
            #
            # Run the following command in the flake's directory to
            # deploy this configuration on any NixOS system:
            #   sudo nixos-rebuild switch --flake .#nixos-test

            "t470p" = nixpkgs.lib.nixosSystem {
                system = "x86_64-linux";

                # The Nix module system can modularize configuration,
                # improving the maintainability of configuration.
                #
                # Each parameter in the `modules` is a Nixpkgs Module, and
                # there is a partial introduction to it in the nixpkgs manual:
                #    <https://nixos.org/manual/nixpkgs/unstable/#module-system-introduction>
                # It is said to be partial because the documentation is not
                # complete, only some simple introductions.
                # such is the current state of Nix documentation...
                #
                # A Nixpkgs Module can be an attribute set, or a function that
                # returns an attribute set. By default, if a Nixpkgs Module is a
                # function, this function has the following default parameters:
                #
                #  lib:     the nixpkgs function library, which provides many
                #             useful functions for operating Nix expressions:
                #             https://nixos.org/manual/nixpkgs/stable/#id-1.4
                #  config:  all config options of the current flake, very useful
                #  options: all options defined in all NixOS Modules
                #             in the current flake
                #  pkgs:   a collection of all packages defined in nixpkgs,
                #            plus a set of functions related to packaging.
                #            you can assume its default value is
                #            `nixpkgs.legacyPackages."${system}"` for now.
                #            can be customed by `nixpkgs.pkgs` option
                #  modulesPath: the default path of nixpkgs's modules folder,
                #               used to import some extra modules from nixpkgs.
                #               this parameter is rarely used,
                #               you can ignore it for now.
                #
                # The default parameters mentioned above are automatically
                # generated by Nixpkgs.
                # However, if you need to pass other non-default parameters
                # to the submodules,
                # you'll have to manually configure these parameters using
                # `specialArgs`.
                # you must use `specialArgs` by uncommenting the following line:
                #
                # specialArgs = {...};  # pass custom arguments into all submodules.

                specialArgs = { inherit inputs; };

                modules = [
                    # main nixos configuration file
                    ./hosts/t470p/configuration.nix

                    inputs.home-manager.nixosModules.home-manager
                    {

                        # use the global pkgs that is configured via the system level nixpkgs options
                        home-manager.useGlobalPkgs = true;

                        # install packages to /etc/profiles
                        home-manager.useUserPackages = true;

                        # import home manager settings
                        home-manager.users.poq = import ./hosts/t470p/home.nix;

                        # use home-manager.extraSpecialArgs to pass arguments to home.nix
                        # (different from the node's specialArgs)
                        home-manager.extraSpecialArgs = {inherit inputs;};

                    }
                ];
            };
        };
    };
}
