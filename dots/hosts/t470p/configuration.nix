# klevstul

{ ... }:

{

    # https://nixos.wiki/wiki/FAQ/When_do_I_update_stateVersion
    system.stateVersion = "23.11";

    # ----------
    # imports
    # ----------
    imports = [

        ./hardware-configuration.nix    # host specific hardware configuration
        ./networking.nix                # host specific networking
        ../../modules/system            # system settings and programs

    ];

}
