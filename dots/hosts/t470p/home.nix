# klevstul

# home manager options:
#   search:
#     https://home-manager-options.extranix.com/?query=
#   list
#     https://nix-community.github.io/home-manager/options.xhtml

{ lib, pkgs, inputs, ... }:
{

    # https://nixos.wiki/wiki/FAQ/When_do_I_update_stateVersion
    home.stateVersion = "23.11";

    # import home-manager modules
    imports = [

        ../../modules/fonts
        ../../modules/programs
        ../../modules/filesystem
        (inputs.home-manager-unstable + "/modules/programs/bemenu.nix")

    ];

    # nicely reload system units when changing configs
    systemd.user.startServices = "sd-switch";

    # create directories
    # https://discourse.nixos.org/t/is-it-possible-to-declare-a-directory-creation-in-the-nixos-configuration/27846/3
    systemd.user.tmpfiles.rules = [
        "d /home/poq/nextcloud/syncDir 0755 poq users -"
        "d /home/poq/syncDir 0755 poq users -"
    ];

    # mounts
    # <!><!><!> WARNING: MOUNTS DO NOT WORK! <!><!><!>
    # https://discourse.nixos.org/t/systemd-user-mounts-is-having-no-effect/44128
    systemd.user.mounts = {
        syncDir = {
            Unit = {
                Description = "syncDir mount";
            };
            Mount = {
                What = "/home/poq/nextcloud/syncDir";
                Where = "/home/poq/syncDir";
            };
        };
    };

    # enable home-manager
    programs.home-manager.enable = true;

    # define information about the user and the path(s) it should manage
    home = {
        username = "poq";
        homeDirectory = "/home/poq";
    };

    # enable fontconfig configuration
    # https://nix-community.github.io/home-manager/options.xhtml#opt-fonts.fontconfig.enable
    fonts.fontconfig.enable = true;

    # add user packages
    home.packages = with pkgs; [
    ];

}
