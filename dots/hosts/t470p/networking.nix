# klevstul

# note: networking can not be a system module, as we will not be able to find the options file before the hostname is
# defined (infinite recursion). a system module needs to know the hostname to load the options!

{...}:
{

    networking.hostName = "t470p";
    networking.networkmanager.enable = true;

}
