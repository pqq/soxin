# t470p options :: klevstul

let
in
{

    # system
    _bluetooth              = true;
    _bluetooth_powerOnBoot  = false;
    _keymap                 = "us";
    _locale                 = "en_DK.UTF-8";
    _nvidia                 = true;
    _nvidia_intelBusId      = "PCI:0:2:0";          # lshw -c display
    _nvidia_nvidiaBusId     = "PCI:2:0:0";
    _opengl                 = true;
    _users                  = [ "poq" ];
    _syncDirSrc             = "/home/poq/nextcloud/syncDir"; # use full path (not '~')
    _syncDirTrg             = "/home/poq/syncDir";

    # fonts
    _fonts                  = [ "nerdfonts" ];

    # programs
    _auto-mount             = false;    # automatically mount drives
    _auto-start             = true;     # automatically start miscellaneous programs
    _feh                    = true;     # light-weight image viewer
    _firefox                = true;     # web browser
    _gimp                   = true;     # gnu image manipulation program
    _git                    = true;     # distributed version control system
    _gitkraken              = true;     # git client
    _gpg                    = true;     # gnu privacy guard
    _htop                   = true;     # interactive process viewer
    _hyprland               = true;     # tiling wayland compositor. will also install: bemenu, hypridle, hyprlock, hyprpaper, hyprshot, mako, waybar, wayland-protocols, wayland-utils (wayland-info), wl-clipboard, xwayland
    _jump                   = true;     # cli jump to directory
    _kitty                  = true;     # terminal emulator
    _lapce                  = true;     # ide / code editor
    _lf                     = true;     # file manager
    _mousepad               = true;     # simple text editor
    _mpv                    = true;     # media player
    _neofetch               = true;     # cli system information displayer
    _neovim                 = true;     # editor
    _networkmanagerapplet   = true;     # networkmanager control applet
    _nextcloud-client       = true;     # nextcloud desktop client
    _pavucontrol            = true;     # pulseaudio volume control
    _pcloud                 = true;     # cloud storage
    _pcmanfm                = true;     # file manager
    _playerctl              = true;     # cli for media player control
    _protonvpn              = true;     # official protonvpn app
    _python                 = true;     # programming language
    _reaper                 = true;     # digital audio workstation (daw)
    _shotcut                = true;     # video editor
    _stremio                = true;     # media center / movies / series
    _synology-drive-client  = false;    # sync files and folders in between pc and synology nas
    _tmux                   = true;     # terminal multiplexer
    _trash-cli              = true;     # trashcan cli interface
    _tuxedo-rs              = false;    # rust utility for interacting with tuxedo hardware
    _vscode                 = false;    # code editor
    _wlr-randr              = true;     # xrandr clone for wlroots compositors
    _xdg-utils              = true;     # cli tools to assist applications with misc desktop integration tasks

    # bashrc insert
    #   only meant to be used for program specific bashrc content.
    #   bashrc content is otherwise added in:
    #   `dots/modules/programs/jump/default.nix`
    _bashrc_insert          = ''
        # jump command
        eval "$(jump shell --bind=j)"
    '';

}
