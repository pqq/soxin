# klevstul

{ lib, osConfig, ... }:
let
    inherit (import ../../../hosts/${osConfig.networking.hostName}/options.nix) _syncDir _nextcloudDir _privacyfolderDir;
in
{

    # import files
    home.file = {
        # please note that source files has to be executable before the import
        # https://discourse.nixos.org/t/how-to-make-scripts-imported-via-home-manager-executable/41909
        ".local/bin" = {
            source = ./scripts;
            recursive = true;
        };
        ".local/wp" = {
            source = ./wallpapers;
            recursive = true;
        };
    };

    # home activation - create default folders
    home.activation = {
        init = lib.hm.dag.entryAfter [ "writeBoundary" ] ''
            # create the following directories if they do not exist from before
            mkdir -p ~/privacyFolder    # local folder, not backed up, used for (un)packing private content
        '';
    };

}
