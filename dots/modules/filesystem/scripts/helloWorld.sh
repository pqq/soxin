#! /usr/bin/env nix-shell
#! nix-shell -i bash -p bash

# https://nixos.wiki/wiki/Nix-shell_shebang#Bash

echo "Hello, World! (helloWorld.sh)"
