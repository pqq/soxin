#! /usr/bin/env nix-shell
#! nix-shell -i bash -p bash

# klevstul :: 24.04

this_file_name=`basename "$0"`
echo ":: $this_file_name :: [K] ::"

src_dir=${SYNCDIR}
trg_dir=~/syncDir

echo
echo "\$src_dir: ${src_dir}"
echo "\$trg_dir: ${trg_dir}"
echo

if [[ "${src_dir}" == "" ]]; then
    echo "environment variable SYNCDIR is not set. you can define it and re-run this script. command to set the variable:"
    echo "export SYNCDIR=\"/full/path/to/syncDir\""
    echo ""
    echo "alternatively, you can input the path manually. do you want to manually input the sync dir path? (y/n)"
    read user_input

    if [[ ${user_input} == "y" ]]; then
        echo "enter the full path to the sync directory:"
        read user_input
        src_dir=${user_input}
    else
        echo "set SYNCDIR, then re-run this script." >&2; exit 1
    fi
fi

if [[ "${src_dir}" == "${trg_dir}" ]]; then
    echo "src == trg. there is nothing to do. bye!" >&2; exit 1
fi

if ! [[ -d ${src_dir} ]]; then
    echo "creating non-existing source directory '${src_dir}'"
    mkdir -p ${src_dir}
fi

if ! [[ -d ${trg_dir} ]]; then
    echo "creating non-existing target directory '${trg_dir}'."
    mkdir -p ${trg_dir}
fi

if mount | grep ${trg_dir} > /dev/null; then
    echo "${trg_dir} is already mounted."
    echo "if you want to unmount this directory, please run:"
    echo "umount ${trg_dir}"
else
    echo "mounting source '${src_dir}' to target '${trg_dir}'."

    # example command:
    # sudo mount -o bind --source /media/drive2/nass/lo/pCloudSync --target ~/pCloudSync
    sudo mount -o bind --source ${src_dir} --target ${trg_dir}

    du --human-readable --max-depth=1 ${trg_dir}
fi

echo
