# klevstul

{ pkgs, lib, osConfig, ... }:
let
    inherit (import ../../../hosts/${osConfig.networking.hostName}/options.nix) _fonts;
in
lib.mkIf (builtins.elem "nerdfonts" _fonts ) {
    home.packages = with pkgs; [
        # full nerfonts package
        # alt: "(nerdfonts.override { fonts = [ "FiraCode" "DroidSansMono" ]; })"
        nerdfonts
    ];
}
