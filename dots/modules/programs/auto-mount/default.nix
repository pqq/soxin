# klevstul


# <!><!><!><!><!><!><!><!><!><!><!><!><!><!><!><!><!><!><!><!><!><!><!><!><!><!><!><!><!><!><!><!><!><!><!>
# WARNING / NOTE:
# This does not work. Unsure why. Related post:
# https://discourse.nixos.org/t/is-it-possible-to-run-systemd-units-as-root-via-home-manager/44166
# <!><!><!><!><!><!><!><!><!><!><!><!><!><!><!><!><!><!><!><!><!><!><!><!><!><!><!><!><!><!><!><!><!><!><!>

{ pkgs, lib, osConfig, ... }:
let
    inherit (import ../../../hosts/${osConfig.networking.hostName}/options.nix) _auto-mount _syncDirSrc _syncDirTrg;
in
lib.mkIf (_auto-mount == true) {

    systemd.user.services.auto-mount = {
        Unit = {
            Description = "systemd service that for automatic mounting";
            after = [ "network.target.service" ];
        };
        Install = {
            WantedBy = [ "default.target" ];
        };
        Service = {
            Type = "simple";
            User = "root";
            Group = "root";
            PermissionsStartOnly = true;
            ExecStart = "${pkgs.writeShellScript "auto-mount" ''
                /run/wrappers/bin/mount -o bind --source ${_syncDirSrc} --target ${_syncDirTrg}
            ''}";
        };
    };

}
