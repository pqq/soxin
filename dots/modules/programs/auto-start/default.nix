# klevstul

# view status:
#   systemctl --user status auto-start
#
# nifty:
#   https://haseebmajid.dev/posts/2023-10-08-how-to-create-systemd-services-in-nix-home-manager/
#   https://documentation.suse.com/smart/systems-management/html/systemd-basics/index.html#concept-systemd-targets

{ pkgs, lib, osConfig, ... }:
let
    inherit (import ../../../hosts/${osConfig.networking.hostName}/options.nix) _auto-start;
in
lib.mkIf (_auto-start == true) {

    # note: i looked into starting hyperland automatically as well (as a new service, with "default.target"). however,
    # that led to hyperland starting every time the system was rebuilt. there might be an easy way around this, setting:
    # `Type = "oneshot"; RemainAfterExit = true` in Service.
    # More info at https://superuser.com/questions/1037466/how-to-start-a-systemd-service-after-user-login-and-stop-it-before-user-logout.

    systemd.user.services.auto-start = {
        Unit = {
            Description = "systemd service that starts up programs automatically";
        };
        Install = {
            WantedBy = [ "graphical-session.target" ];
        };
        Service = {

            # https://unix.stackexchange.com/questions/516749/how-best-to-start-my-systemd-service-to-run-multiple-apps
            Type = "forking";
            TimeoutSec = 0;
            RemainAfterExit = "yes";
            GuessMainPID = "no";

            # direct all (channel 2 is errors) to to standard out: 2>&1
            # https://stackoverflow.com/questions/418896/how-to-redirect-output-to-a-file-and-stdout
            ExecStart = "${pkgs.writeShellScript "auto-start" ''

                ${pkgs.hyprdim}/bin/hyprdim --strength 0.6 --duration 500 --no-dim-when-only --dialog-dim 0.1 &
                ${pkgs.hyprpaper}/bin/hyprpaper &
                ${pkgs.waybar}/bin/waybar &

                # the following programs does not exist in pkgs, hence hardcoded paths:
                /etc/profiles/per-user/poq/bin/hypridle &
                /etc/profiles/per-user/poq/bin/pcloud &

            ''}";
        };
    };

}
