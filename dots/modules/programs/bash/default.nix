# klevstul #

{ pkgs, osConfig, ... }:
let
    inherit (import ../../../hosts/${osConfig.networking.hostName}/options.nix) _bashrc_insert;
in
{

    # bash :: https://home-manager-options.extranix.com/?query=bash
    programs.bash.enable = true;

    # write shell scripts
    home.packages = with pkgs; [

        (pkgs.writeShellScriptBin "helloWorld_1" ''
            echo "Hello, World! (pkgs.writeShellScriptBin)";
        '')

        (pkgs.writeShellScriptBin "helloWorld_2"
            (builtins.readFile ../../filesystem/scripts/helloWorld.sh)
        )

        (pkgs.writeShellScriptBin "syncDirSetup"
            (builtins.readFile ../../filesystem/scripts/syncDirSetup.sh)
        )

    ];

    # extra commands to run when initializing an interactive shell
    programs.bash.initExtra = ''

        helloWorld_3 () {
            echo "Hello, World! (programs.bash.initExtra)";
        }

        # -----
        # sys stuff
        # -----

        # -----
        # bashrc insert
        # -----
        ${_bashrc_insert}

    '';

    # extra commands placed in ~/.bashrc (will run even in non-interactive shells)
    programs.bash.bashrcExtra = ''
    '';

}

# OLD BASHRC COMMANDS BELOW. MOVE THEM UP WHEN/IF NEEDED!

#    # -----------------------
#    # own additions below
#    #
#    # note: only use lowercase letters for aliases!
#    # -----------------------
#
#    # ---
#    # sys stuff
#    # ---
#    eval "$(jump shell)"
#    alias genesis='~/Scripts/genesis.sh'
#    alias curae='~/Scripts/tmp/curae.sh'
#
#    alias nas-mount="sudo systemctl start media-nas.mount"
#    alias nas-unmount="sudo systemctl stop media-nas.mount"
#
#    iso-mount() {
#    	sudo mount -o loop "$1" /media/iso
#    }
#    alias iso-unmount="sudo umount /media/iso"
#
#    # https://en.wikipedia.org/wiki/List_of_common_resolutions
#    alias res-low="xrandr -s 1920x1080"
#    alias res-med="xrandr -s 2560x1440"
#    alias res-high="xrandr -s 3840x2160"
#
#    # ---
#    # misc
#    # ---
#    downstream() {
#        output="output.mp4"
#    	# https://stackoverflow.com/questions/3601515/how-to-check-if-a-variable-is-set-in-bash
#    	if [[ -n $2 ]]; then
#    		output=$2
#    	fi
#    	ffmpeg -i "$1" -c copy -bsf:a aac_adtstoasc "/media/drive2/nass/lo/0_downloads/${output}"
#    }
#    alias videoCompressor="/media/drive2/nass/cb/gitRepos/git.mz.fo/miniProjects/2104_videoCompressor/src/vc_v2.sh"
#    alias videoWatermark="/media/drive2/nass/cb/gitRepos/git.mz.fo/miniProjects/2104_videoCompressor/src/vcwm_v1.sh"
#    alias webserver-start="/media/drive2/nass/cb/gitRepos/git.mz.fo/localWebServer/sh/startWebserver.sh"
#    gpx2jon() {
#    	cd /media/drive2/nass/cb/gitRepos/git.mz.fo/gpx2jon/src
#    	source venv/bin/activate
#    	python archi.py -s /media/drive2/nass/cb/gitRepos/git.mz.fo/fiodb/db/gpx2jon/sites
#    	deactivate
#    }
#    gpx2jon-with-build-all() {
#    	gpx2jon
#    	frodr.com-build-all
#    }
#    alias imgResize='/media/drive2/nass/cb/gitRepos/git.mz.fo/miniProjects/2305_imgResizer/resize.sh 1200'
#    alias notesArchiver='/media/drive2/nass/cb/gitRepos/git.mz.fo/miniProjects/2306_notesArchiver/notesArchiver.sh'
#    alias toJpg='/media/drive2/nass/cb/gitRepos/git.mz.fo/miniProjects/2306_toJpg/toJpg.sh'
#    alias gpxImporter='python /media/drive2/nass/cb/gitRepos/git.mz.fo/miniProjects/2308_gpxImporter/importer.py'
#
#    # ---
#    # ajp related
#    # ---
#    ajp-cp2dropbox() {
#    	"/media/drive2/nass/cb/gitRepos/git.mz.fo/miniProjects/2106_ajpDropboxDeployer/archi.sh" "$1" "$2"
#    }
#    podigy() {
#    	cd /media/drive2/nass/cb/gitRepos/git.mz.fo/podigy/src
#    	source venv/bin/activate
#    	python archi.py -s /media/drive2/nass/cb/gitRepos/git.mz.fo/fiodb/db/podigy/sites
#    	deactivate
#    }
#    podigy-pywsdb() {
#    	podigy
#    	/media/drive2/nass/cb/gitRepos/git.mz.fo/pyws/deploy/deployToBuildServer.sh db
#    }
#
#    # ---
#    # c19vaxno
#    # ---
#    #alias c19vaxno-deploy="/media/drive2/nass/cb/gitRepos/gitlab/c19vaxno/helperScripts/deployToGitlab.sh"
#
#    # ---
#    # hummed
#    # ---
#    hummed-activate() {
#    	cd /media/drive2/nass/cb/gitRepos/git.mz.fo/hummed/src
#    	source venv/bin/activate
#    }
#
#    hummed() {
#        hummed-activate
#        python /media/drive2/nass/cb/gitRepos/git.mz.fo/hummed/src/archi.py "$@"
#        deactivate
#        jump-to-downloads
#    }
#
#    # ---
#    # jeton
#    # ---
#    jeton() {
#    	cd /media/drive2/nass/cb/gitRepos/git.mz.fo/jeton/src/
#    	source venv/bin/activate
#    	python archi.py -s /media/drive2/nass/cb/gitRepos/git.mz.fo/fiodb/db/jeton/in/sites -r $1
#    	deactivate
#    }
#
#    # ---
#    # server syncs/backups/deploys
#    # ---
#    srv-guru-backup() {
#    	echo "[srv-guru-backup]: <=== download wc2sd/exec/"
#    	rsync -v -a --progress --stats --e 'ssh -p 1808' trunk@135.181.193.110:/home/trunk/wc2sd/exec/ /media/drive2/nass/cb/gitRepos/git.mz.fo/srv.guru/trunk/wc2sd/exec/
#    	echo "[srv-guru-backup]: <=== download wc2sd/db/"
#    	rsync -v -a --progress --stats -e 'ssh -p 1808' trunk@135.181.193.110:/home/trunk/wc2sd/db/ /media/drive2/nass/cb/gitRepos/git.mz.fo/srv.guru/trunk/wc2sd/db/
#    	echo "[srv-guru-backup]: <=== download cron_export/"
#    	rsync -v -a --progress --stats -e 'ssh -p 1808' trunk@135.181.193.110:/home/trunk/cron_export/ /media/drive2/nass/cb/gitRepos/git.mz.fo/srv.guru/trunk/cron_export/
#    	echo "[srv-guru-backup]: <=== download .ssh/"
#    	rsync -v -a --progress --stats -e 'ssh -p 1808' trunk@135.181.193.110:/home/trunk/.ssh/ /media/drive2/nass/cb/gitRepos/git.mz.fo/srv.guru/trunk/.ssh/
#    }
#
#    # ---
#    # misc websites
#    # ---
#    alias 00101111.xyz-deploy="rsync -v -a --progress --stats --delete-delay -e 'ssh -p 1808' /media/drive2/nass/cb/gitRepos/git.mz.fo/00101111.xyz/www/ trunk@167.235.77.26:/var/www/00101111.xyz/"
#    alias mz.fo-deploy="rsync -v -a --progress --stats --delete-delay -e 'ssh -p 1808' /media/drive2/nass/cb/gitRepos/git.mz.fo/mz.fo/www/ trunk@167.235.77.26:/var/www/mz.fo/"
#
#    # ---
#    # pusterom.com
#    # ---
#    alias pusterom.com-themeUpd="rsync -v -a --progress --stats --delete-delay -e 'ssh -p 22' /media/drive2/nass/cb/gitRepos/git.mz.fo/pusterom-hht/src/Pusterom/ root@209.38.217.93:/var/www/humhub/themes/Pusterom/"
#
#    # ---
#    # jongleur
#    # ---
#    jcssbuilder() {
#    	cd /media/drive2/nass/cb/gitRepos/git.mz.fo/jCssBuilder/src
#    	source venv/bin/activate
#    	python /media/drive2/nass/cb/gitRepos/git.mz.fo/jCssBuilder/src/archi.py
#    	deactivate
#    }
#    jongleur-activate() {
#    	cd /media/drive2/nass/cb/gitRepos/gitlab/jongleur/src
#    	source venv/bin/activate
#    }
#    jump-to-downloads() {
#        cd /media/drive2/nass/lo/0_downloads
#    }
#    ajp.fm-build() {
#    	jongleur-activate
#    	/media/drive2/nass/cb/gitRepos/git.mz.fo/ajp.fm/jongleur/run.sh
#    	deactivate
#        jump-to-downloads
#    }
#    ajp.fm-build-all() {
#    	jongleur-activate
#    	/media/drive2/nass/cb/gitRepos/git.mz.fo/ajp.fm/jongleur/runAll.sh
#    	deactivate
#        jump-to-downloads
#    }
#    alias ajp.fm-deploy="rsync -v -a --progress --stats --delete-delay -e 'ssh -p 1808' /media/drive2/nass/cb/gitRepos/git.mz.fo/ajp.fm/jongleur/html_export/ trunk@167.235.77.26:/var/www/ajp.fm/jongleur/"
#    antijanteboka.com-build() {
#    	jongleur-activate
#    	/media/drive2/nass/cb/gitRepos/git.mz.fo/antijanteboka.com/jongleur/run.sh
#    	deactivate
#        jump-to-downloads
#    }
#    antijanteboka.com-build-all() {
#    	jongleur-activate
#    	/media/drive2/nass/cb/gitRepos/git.mz.fo/antijanteboka.com/jongleur/runAll.sh
#    	deactivate
#        jump-to-downloads
#    }
#    alias antijanteboka.com-deploy="rsync -v -a --progress --stats --delete-delay -e 'ssh -p 1808' /media/drive2/nass/cb/gitRepos/git.mz.fo/antijanteboka.com/jongleur/html_export/ trunk@167.235.77.26:/var/www/antijanteboka.com/jongleur/"
#    antijantemiriam.com-build() {
#    	jongleur-activate
#    	/media/drive2/nass/cb/gitRepos/git.mz.fo/antijantemiriam.com/jongleur/run.sh
#    	deactivate
#        jump-to-downloads
#    }
#    antijantemiriam.com-build-all() {
#    	jongleur-activate
#    	/media/drive2/nass/cb/gitRepos/git.mz.fo/antijantemiriam.com/jongleur/runAll.sh
#    	deactivate
#        jump-to-downloads
#    }
#    alias antijantemiriam.com-deploy="rsync -v -a --progress --stats --delete-delay -e 'ssh -p 1808' /media/drive2/nass/cb/gitRepos/git.mz.fo/antijantemiriam.com/jongleur/html_export/ trunk@167.235.77.26:/var/www/antijantemiriam.com/jongleur/"
#    billgoats.com-build() {
#    	jongleur-activate
#    	/media/drive2/nass/cb/gitRepos/git.mz.fo/billgoats.com/jongleur/run.sh
#    	deactivate
#        jump-to-downloads
#    }
#    billgoats.com-build-all() {
#    	jongleur-activate
#    	/media/drive2/nass/cb/gitRepos/git.mz.fo/billgoats.com/jongleur/runAll.sh
#    	deactivate
#        jump-to-downloads
#    }
#    alias billgoats.com-deploy="rsync -v -a --progress --stats --delete-delay -e 'ssh -p 1808' /media/drive2/nass/cb/gitRepos/git.mz.fo/billgoats.com/jongleur/html_export/ trunk@167.235.77.26:/var/www/billgoats.com/jongleur/"
#    curious.art-build() {
#    	jongleur-activate
#    	/media/drive2/nass/cb/gitRepos/git.mz.fo/curious.art/jongleur/run.sh
#    	deactivate
#        jump-to-downloads
#    }
#    curious.art-build-all() {
#    	jongleur-activate
#    	/media/drive2/nass/cb/gitRepos/git.mz.fo/curious.art/jongleur/runAll.sh
#    	deactivate
#        jump-to-downloads
#    }
#    alias curious.art-deploy="rsync -v -a --progress --stats --delete-delay -e 'ssh -p 1808' /media/drive2/nass/cb/gitRepos/git.mz.fo/curious.art/jongleur/html_export/ trunk@167.235.77.26:/var/www/curious.art/jongleur/"
#    curiouscreators.com-build() {
#    	jongleur-activate
#    	/media/drive2/nass/cb/gitRepos/git.mz.fo/curiouscreators.com/jongleur/run.sh
#    	deactivate
#        jump-to-downloads
#    }
#    curiouscreators.com-build-all() {
#    	jongleur-activate
#    	/media/drive2/nass/cb/gitRepos/git.mz.fo/curiouscreators.com/jongleur/runAll.sh
#    	deactivate
#        jump-to-downloads
#    }
#    alias curiouscreators.com-deploy="rsync -v -a --progress --stats --delete-delay -e 'ssh -p 1808' /media/drive2/nass/cb/gitRepos/git.mz.fo/curiouscreators.com/jongleur/html_export/ trunk@167.235.77.26:/var/www/curiouscreators.com/jongleur/"
#    frodr.com-build() {
#    	jongleur-activate
#    	/media/drive2/nass/cb/gitRepos/git.mz.fo/frodr.com/jongleur/run.sh
#    	deactivate
#        jump-to-downloads
#    }
#    frodr.com-build-all() {
#    	jongleur-activate
#    	/media/drive2/nass/cb/gitRepos/git.mz.fo/frodr.com/jongleur/runAll.sh
#    	deactivate
#        jump-to-downloads
#    }
#    alias frodr.com-deploy="rsync -v -a --progress --stats --delete-delay -e 'ssh -p 1808' /media/drive2/nass/cb/gitRepos/git.mz.fo/frodr.com/jongleur/html_export/ trunk@167.235.77.26:/var/www/frodr.com/jongleur/"
#    klevstul.com-build() {
#    	jongleur-activate
#    	/media/drive2/nass/cb/gitRepos/git.mz.fo/klevstul.com/jongleur/run.sh
#    	deactivate
#        jump-to-downloads
#    }
#    klevstul.com-build-all() {
#    	jongleur-activate
#    	/media/drive2/nass/cb/gitRepos/git.mz.fo/klevstul.com/jongleur/runAll.sh
#    	deactivate
#        jump-to-downloads
#    }
#    alias klevstul.com-deploy="rsync -v -a --progress --stats --delete-delay -e 'ssh -p 1808' /media/drive2/nass/cb/gitRepos/git.mz.fo/klevstul.com/jongleur/html_export/ trunk@167.235.77.26:/var/www/klevstul.com/jongleur/"
#