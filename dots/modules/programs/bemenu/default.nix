# klevstul

# https://github.com/Cloudef/bemenu
# https://home-manager-options.extranix.com/?query=bemenu&release=master

{ pkgs, lib, osConfig, ... }:
let
    inherit (import ../../../hosts/${osConfig.networking.hostName}/options.nix) _hyprland;
in
lib.mkIf (_hyprland == true) {

    # installing from nixpkgs:
    #home.packages = with pkgs; [
    #    bemenu
    #];

    # currently only in the home-manager unstable branch
    # so, in home.nix an specific import of bemenu.nix is done
    programs.bemenu.enable = true;
    #programs.bemenu.settings = {};

}
