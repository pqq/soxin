# programs module :: klevstul

{...}:
{

    imports = [

        ./auto-mount
        ./auto-start
        ./bash
        ./bemenu
        ./feh
        ./firefox
        ./gimp
        ./git
        ./gitkraken
        ./gpg
        ./htop
        ./hyprdim
        ./hypridle
        ./hyprland
        ./hyprlock
        ./hyprpaper
        ./hyprshot
        ./jump
        ./kitty
        ./lapce
        ./lf
        ./mako
        ./mousepad
        ./mpv
        ./neofetch
        ./neovim
        ./networkmanagerapplet
        ./nextcloud-client
        ./pavucontrol
        ./pcloud
        ./pcmanfm
        ./playerctl
        ./protonvpn-gui
        ./python
        ./reaper
        ./shotcut
        ./stremio
        ./synology-drive-client
        ./tmux
        ./trash-cli
        ./vscode
        ./waybar
        ./wayland-protocols
        ./wayland-utils
        ./wl-clipboard
        ./wlr-randr
        ./xdg-utils
        ./xwayland

    ];

}
