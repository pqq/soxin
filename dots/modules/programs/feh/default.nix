# klevstul

# https://feh.finalrewind.org/

{ pkgs, lib, osConfig, ... }:
let
    inherit (import ../../../hosts/${osConfig.networking.hostName}/options.nix) _feh;
in
lib.mkIf (_feh == true) {

    programs.feh.enable = true;

}
