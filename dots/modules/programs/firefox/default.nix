# klevstul

# https://home-manager-options.extranix.com/?query=firefox&release=release-23.11
# https://mynixos.com/home-manager/options/programs.firefox

# NOTE:
# you might have to delete "~/.mozilla/firefox/profiles.ini" if that files exists before first build
# this would have happened in case you have installed firefox manually. it will cause a build fail.

{ lib, osConfig, ... }:
let
    inherit (import ../../../hosts/${osConfig.networking.hostName}/options.nix) _firefox;
in
lib.mkIf (_firefox == true) {

    programs.firefox.enable = true;

    programs.firefox.policies = {

        # https://mozilla.github.io/policy-templates/

        DisableFirefoxStudies       = true; # never run SHIELD studies or do Heartbeat surveys
        DisablePocket               = true;
        DisableTelemetry            = true;
        DisplayBookmarksToolbar     = "never";          # {always, never, newtab}
        DisplayMenuBar              = "default-off";    # {always, default-off, default-on, never}
        DontCheckDefaultBrowser     = true;
        EnableTrackingProtection    = {
            Value                   = true;
            Locked                  = true;
            Cryptomining            = true;
            Fingerprinting          = true;
        };
        OverrideFirstRunPage        = "";               # specify a URL to be used as the first run page
        OverridePostUpdatePage      = "";               # specify a URL to be displayed after Firefox is updated
        SearchBar                   = "unified";        # {separate, unified}

    };

    programs.firefox.profiles.poq.userChrome = ''

        /* disable top bar and top tabs */
        #TabsToolbar {
            visibility: collapse;
        }
        #titlebar {
            display: none;
        }

    '';

    programs.firefox.profiles.poq.settings = {

        # https://kb.mozillazine.org/About:config_entries
        # https://support.mozilla.org/bm/questions/1358615

        "browser.bookmarks.restore_default_bookmarks" = false;
        "browser.bookmarks.showMobileBookmarks" = true;
        "browser.compactmode.show" = true;
        "browser.ctrlTab.sortByRecentlyUsed" = true;
        "browser.download.always_ask_before_handling_new_types" = true;
        "browser.download.alwaysOpenPanel" = false;
        "browser.download.dir" = "~/Downloads";
        "browser.download.folderList" = 2;
        "browser.download.lastDir" = "~/Downloads";
        "browser.download.panel.shown" = true;
        "browser.download.save_converter_index" = 0;
        "browser.download.useDownloadDir" = false;
        "browser.formfill.enable" = false;
        "browser.newtabpage.activity-stream.topSitesRows" = 4;
        "browser.newtabpage.pinned" = [{title = "NixOS"; url = "https://nixos.org";}{title = "frodr";url = "https://frodr.com";}];
        "browser.open.lastDir" = "~/Downloads";
        "browser.search.isUS" = false;
        "browser.search.region" = "GB";
        "browser.startup.homepage" = "https://mz.fo";
        "browser.startup.page" = 3;
        "browser.tabs.inTitlebar" = 0;
        "browser.toolbars.bookmarks.visibility" = "never";
        "browser.translations.panelShown" = true;
        "browser.uiCustomization.state" = "{\"placements\":{\"widget-overflow-fixed-list\":[\"fxa-toolbar-menu-button\",\"sidebar-button\",\"sync-button\"],\"unified-extensions-area\":[\"cloudfirewall_nikisoft_one-browser-action\",\"_60f82f00-9ad5-4de5-b31c-b16a47c51558_-browser-action\",\"_d07ccf11-c0cd-4938-a265-2a4d6ad01189_-browser-action\",\"foxyproxy_eric_h_jung-browser-action\",\"_84b703a4-eaff-4208-9cee-12c0623056c1_-browser-action\",\"_c2c003ee-bd69-42a2-b0e9-6f34222cb046_-browser-action\",\"addon_simplelogin-browser-action\",\"_74145f27-f039-47ce-a470-a662b129930a_-browser-action\",\"_799c0914-748b-41df-a25c-22d008f9e83f_-browser-action\",\"_48748554-4c01-49e8-94af-79662bf34d50_-browser-action\",\"jid1-kkzogwgsw3ao4q_jetpack-browser-action\",\"customscrollbars_computerwhiz-browser-action\",\"_76b6ec7e-5411-4d36-9aa5-2bb08e31c27e_-browser-action\",\"_b9db16a4-6edc-47ec-a1f4-b86292ed211d_-browser-action\",\"_441ae0e7-c955-4b0e-8209-99462af458db_-browser-action\",\"_contain-facebook-browser-action\",\"copyplaintext_eros_man-browser-action\",\"_56b215f4-29b6-4898-bf2a-152d8bc189ed_-browser-action\",\"_3c078156-979c-498b-8990-85f7987dd929_-browser-action\"],\"nav-bar\":[\"back-button\",\"forward-button\",\"stop-reload-button\",\"home-button\",\"urlbar-container\",\"support_lastpass_com-browser-action\",\"_446900e4-71c2-419f-a6a7-df9c091e268b_-browser-action\",\"authenticator_mymindstorm-browser-action\",\"sqrl_pass_dog-browser-action\",\"ublock0_raymondhill_net-browser-action\",\"vpn_proton_ch-browser-action\",\"customizableui-special-spring27\",\"emoji_saveriomorelli_com-browser-action\",\"_8397abea-6e82-4bb3-ad79-243bf490892a_-browser-action\",\"_7a7b1d36-d7a4-481b-92c6-9f5427cb9eb1_-browser-action\",\"_5b22cb75-8e43-4f2a-bb9b-1da0655ae564_-browser-action\",\"_531906d3-e22f-4a6c-a102-8057b88a1a63_-browser-action\",\"_8419486a-54e9-11e8-9401-ac9e17909436_-browser-action\",\"jid0-gxjllfbcoax0lcltedfrekqdqpi_jetpack-browser-action\",\"_b6bd7e35-0762-42a2-a283-95a94635047d_-browser-action\",\"_9350bc42-47fb-4598-ae0f-825e3dd9ceba_-browser-action\",\"customizableui-special-spring22\",\"_e9b7227c-28db-4739-a175-36679a55bc4e_-browser-action\",\"private-relay_firefox_com-browser-action\",\"87677a2c52b84ad3a151a4a72f5bd3c4_jetpack-browser-action\",\"downloads-button\",\"_c580e2d6-885a-4cb9-af49-59b3b48350e9_-browser-action\",\"verticaltabsreloaded_go-dev_de-browser-action\",\"_48df221a-8316-4d17-9191-7fc5ea5f14c0_-browser-action\",\"history-panelmenu\",\"https-everywhere-eff_eff_org-browser-action\",\"browser-extension_anonaddy-browser-action\",\"_61173a74-ece7-4ef3-86a7-525538b78430_-browser-action\",\"reset-pbm-toolbar-button\",\"unified-extensions-button\"],\"toolbar-menubar\":[\"menubar-items\"],\"TabsToolbar\":[\"firefox-view-button\",\"tabbrowser-tabs\",\"new-tab-button\",\"alltabs-button\"],\"PersonalToolbar\":[]},\"seen\":[\"developer-button\",\"_testpilot-containers-browser-action\",\"_c2c003ee-bd69-42a2-b0e9-6f34222cb046_-browser-action\",\"_contain-facebook-browser-action\",\"87677a2c52b84ad3a151a4a72f5bd3c4_jetpack-browser-action\",\"_e9b7227c-28db-4739-a175-36679a55bc4e_-browser-action\",\"sqrl_pass_dog-browser-action\",\"verticaltabsreloaded_go-dev_de-browser-action\",\"_c580e2d6-885a-4cb9-af49-59b3b48350e9_-browser-action\",\"_48df221a-8316-4d17-9191-7fc5ea5f14c0_-browser-action\",\"support_lastpass_com-browser-action\",\"jid1-kkzogwgsw3ao4q_jetpack-browser-action\",\"ublock0_raymondhill_net-browser-action\",\"firefoxcolor_mozilla_com-browser-action\",\"_9350bc42-47fb-4598-ae0f-825e3dd9ceba_-browser-action\",\"authenticator_mymindstorm-browser-action\",\"jid0-gxjllfbcoax0lcltedfrekqdqpi_jetpack-browser-action\",\"private-relay_firefox_com-browser-action\",\"foxyproxy_eric_h_jung-browser-action\",\"_3c078156-979c-498b-8990-85f7987dd929_-browser-action\",\"_531906d3-e22f-4a6c-a102-8057b88a1a63_-browser-action\",\"_60f82f00-9ad5-4de5-b31c-b16a47c51558_-browser-action\",\"_b6bd7e35-0762-42a2-a283-95a94635047d_-browser-action\",\"_d07ccf11-c0cd-4938-a265-2a4d6ad01189_-browser-action\",\"_48748554-4c01-49e8-94af-79662bf34d50_-browser-action\",\"https-everywhere-eff_eff_org-browser-action\",\"_8419486a-54e9-11e8-9401-ac9e17909436_-browser-action\",\"_446900e4-71c2-419f-a6a7-df9c091e268b_-browser-action\",\"_799c0914-748b-41df-a25c-22d008f9e83f_-browser-action\",\"cloudfirewall_nikisoft_one-browser-action\",\"_74145f27-f039-47ce-a470-a662b129930a_-browser-action\",\"copyplaintext_eros_man-browser-action\",\"jid1-nmve2op40qeqdq_jetpack-browser-action\",\"_5b22cb75-8e43-4f2a-bb9b-1da0655ae564_-browser-action\",\"_7a7b1d36-d7a4-481b-92c6-9f5427cb9eb1_-browser-action\",\"addon_simplelogin-browser-action\",\"_84b703a4-eaff-4208-9cee-12c0623056c1_-browser-action\",\"customscrollbars_computerwhiz-browser-action\",\"_76b6ec7e-5411-4d36-9aa5-2bb08e31c27e_-browser-action\",\"browser-extension_anonaddy-browser-action\",\"emoji_saveriomorelli_com-browser-action\",\"_b9db16a4-6edc-47ec-a1f4-b86292ed211d_-browser-action\",\"_61173a74-ece7-4ef3-86a7-525538b78430_-browser-action\",\"_8397abea-6e82-4bb3-ad79-243bf490892a_-browser-action\",\"_441ae0e7-c955-4b0e-8209-99462af458db_-browser-action\",\"vpn_proton_ch-browser-action\",\"_56b215f4-29b6-4898-bf2a-152d8bc189ed_-browser-action\"],\"dirtyAreaCache\":[\"nav-bar\",\"toolbar-menubar\",\"TabsToolbar\",\"PersonalToolbar\",\"widget-overflow-fixed-list\",\"unified-extensions-area\"],\"currentVersion\":20,\"newElementCount\":47}";
        "browser.uidensity" = 1;
        "browser.urlbar.doubleClickSelectsAll" = false;
        "browser.urlbar.placeholderName" = "-_-";
        "browser.urlbar.showSearchSuggestionsFirst" = false;
        "devtools.cache.disabled" = true;
        "devtools.everOpened" = true;
        "distribution.searchplugins.defaultLocale" = "en-GB";
        "dom.security.https_only_mode" = true;
        "dom.webnotifications.enabled" = true;
        "extensions.activeThemeID" = "activist-bold-colorway@mozilla.org";
        "extensions.formautofill.creditCards.enabled" = false;
        "extensions.pocket.enabled" = false;
        "fission.autostart" = true;
        "general.useragent.locale" = "en-GB";
        "identity.fxaccounts.account.device.name" = "poq @ {hostname}";
        "intl.accept_languages" = "en";
        "layout.css.prefers-color-scheme.content-override" = 1;
        "privacy.donottrackheader.enabled" = true;
        "privacy.fingerprintingProtection.overrides" = "+AllTargets,-JSDateTimeUTC";
        "privacy.fingerprintingProtection" = true;
        "privacy.resistFingerprinting" = false;
        "signon.rememberSignons" = false;                                               # do not save passwords
        "toolkit.legacyUserProfileCustomizations.stylesheets" = true;

        #"browser.contentblocking.category" = "standard";
        #"browser.contentblocking.cfr-milestone.milestone-achieved" = 10000;
        #"browser.contentblocking.cfr-milestone.milestone-shown-time" = 1686905452439;
        #"browser.contextual-services.contextId" = "{d83fbb3c-6408-43ba-b003-225d78660092}";
        #"browser.eme.ui.firstContentShown" = true;
        #"browser.engagement.ctrlTab.has-used" = true;
        #"browser.engagement.downloads-button.has-used" = true;
        #"browser.engagement.fxa-toolbar-menu-button.has-used" = true;
        #"browser.engagement.home-button.has-used" = true;
        #"browser.uiCustomization.state" = "{\"placements\":{\"widget-overflow-fixed-list\":[\"_74145f27-f039-47ce-a470-a662b129930a_-browser-action\",\"_799c0914-748b-41df-a25c-22d008f9e83f_-browser-action\",\"_48748554-4c01-49e8-94af-79662bf34d50_-browser-action\",\"jid1-kkzogwgsw3ao4q_jetpack-browser-action\",\"customscrollbars_computerwhiz-browser-action\",\"fxa-toolbar-menu-button\",\"_76b6ec7e-5411-4d36-9aa5-2bb08e31c27e_-browser-action\",\"_b9db16a4-6edc-47ec-a1f4-b86292ed211d_-browser-action\"],\"nav-bar\":[\"back-button\",\"forward-button\",\"stop-reload-button\",\"home-button\",\"urlbar-container\",\"support_lastpass_com-browser-action\",\"_446900e4-71c2-419f-a6a7-df9c091e268b_-browser-action\",\"authenticator_mymindstorm-browser-action\",\"addon_simplelogin-browser-action\",\"sqrl_pass_dog-browser-action\",\"customizableui-special-spring27\",\"emoji_saveriomorelli_com-browser-action\",\"_c2c003ee-bd69-42a2-b0e9-6f34222cb046_-browser-action\",\"_8397abea-6e82-4bb3-ad79-243bf490892a_-browser-action\",\"ublock0_raymondhill_net-browser-action\",\"_7a7b1d36-d7a4-481b-92c6-9f5427cb9eb1_-browser-action\",\"_5b22cb75-8e43-4f2a-bb9b-1da0655ae564_-browser-action\",\"_531906d3-e22f-4a6c-a102-8057b88a1a63_-browser-action\",\"_84b703a4-eaff-4208-9cee-12c0623056c1_-browser-action\",\"_8419486a-54e9-11e8-9401-ac9e17909436_-browser-action\",\"foxyproxy_eric_h_jung-browser-action\",\"jid0-gxjllfbcoax0lcltedfrekqdqpi_jetpack-browser-action\",\"_60f82f00-9ad5-4de5-b31c-b16a47c51558_-browser-action\",\"_b6bd7e35-0762-42a2-a283-95a94635047d_-browser-action\",\"_d07ccf11-c0cd-4938-a265-2a4d6ad01189_-browser-action\",\"_9350bc42-47fb-4598-ae0f-825e3dd9ceba_-browser-action\",\"cloudfirewall_nikisoft_one-browser-action\",\"customizableui-special-spring22\",\"_e9b7227c-28db-4739-a175-36679a55bc4e_-browser-action\",\"private-relay_firefox_com-browser-action\",\"87677a2c52b84ad3a151a4a72f5bd3c4_jetpack-browser-action\",\"downloads-button\",\"_c580e2d6-885a-4cb9-af49-59b3b48350e9_-browser-action\",\"verticaltabsreloaded_go-dev_de-browser-action\",\"_48df221a-8316-4d17-9191-7fc5ea5f14c0_-browser-action\",\"history-panelmenu\",\"https-everywhere-eff_eff_org-browser-action\",\"browser-extension_anonaddy-browser-action\",\"_61173a74-ece7-4ef3-86a7-525538b78430_-browser-action\"],\"toolbar-menubar\":[\"menubar-items\"],\"TabsToolbar\":[\"tabbrowser-tabs\",\"new-tab-button\",\"alltabs-button\"],\"PersonalToolbar\":[]},\"seen\":[\"developer-button\",\"_testpilot-containers-browser-action\",\"_c2c003ee-bd69-42a2-b0e9-6f34222cb046_-browser-action\",\"_contain-facebook-browser-action\",\"87677a2c52b84ad3a151a4a72f5bd3c4_jetpack-browser-action\",\"_e9b7227c-28db-4739-a175-36679a55bc4e_-browser-action\",\"sqrl_pass_dog-browser-action\",\"verticaltabsreloaded_go-dev_de-browser-action\",\"_c580e2d6-885a-4cb9-af49-59b3b48350e9_-browser-action\",\"_48df221a-8316-4d17-9191-7fc5ea5f14c0_-browser-action\",\"support_lastpass_com-browser-action\",\"jid1-kkzogwgsw3ao4q_jetpack-browser-action\",\"ublock0_raymondhill_net-browser-action\",\"firefoxcolor_mozilla_com-browser-action\",\"_9350bc42-47fb-4598-ae0f-825e3dd9ceba_-browser-action\",\"authenticator_mymindstorm-browser-action\",\"jid0-gxjllfbcoax0lcltedfrekqdqpi_jetpack-browser-action\",\"private-relay_firefox_com-browser-action\",\"foxyproxy_eric_h_jung-browser-action\",\"_3c078156-979c-498b-8990-85f7987dd929_-browser-action\",\"_531906d3-e22f-4a6c-a102-8057b88a1a63_-browser-action\",\"_60f82f00-9ad5-4de5-b31c-b16a47c51558_-browser-action\",\"_b6bd7e35-0762-42a2-a283-95a94635047d_-browser-action\",\"_d07ccf11-c0cd-4938-a265-2a4d6ad01189_-browser-action\",\"_48748554-4c01-49e8-94af-79662bf34d50_-browser-action\",\"https-everywhere-eff_eff_org-browser-action\",\"_8419486a-54e9-11e8-9401-ac9e17909436_-browser-action\",\"_446900e4-71c2-419f-a6a7-df9c091e268b_-browser-action\",\"_799c0914-748b-41df-a25c-22d008f9e83f_-browser-action\",\"cloudfirewall_nikisoft_one-browser-action\",\"_74145f27-f039-47ce-a470-a662b129930a_-browser-action\",\"copyplaintext_eros_man-browser-action\",\"jid1-nmve2op40qeqdq_jetpack-browser-action\",\"_5b22cb75-8e43-4f2a-bb9b-1da0655ae564_-browser-action\",\"_7a7b1d36-d7a4-481b-92c6-9f5427cb9eb1_-browser-action\",\"addon_simplelogin-browser-action\",\"_84b703a4-eaff-4208-9cee-12c0623056c1_-browser-action\",\"customscrollbars_computerwhiz-browser-action\",\"_76b6ec7e-5411-4d36-9aa5-2bb08e31c27e_-browser-action\",\"browser-extension_anonaddy-browser-action\",\"emoji_saveriomorelli_com-browser-action\",\"_b9db16a4-6edc-47ec-a1f4-b86292ed211d_-browser-action\",\"_61173a74-ece7-4ef3-86a7-525538b78430_-browser-action\",\"_8397abea-6e82-4bb3-ad79-243bf490892a_-browser-action\"],\"dirtyAreaCache\":[\"nav-bar\",\"toolbar-menubar\",\"TabsToolbar\",\"PersonalToolbar\",\"widget-overflow-fixed-list\"],\"currentVersion\":17,\"newElementCount\":43}";
        #"services.sync.username" = "";

    };

}
