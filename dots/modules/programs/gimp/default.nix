# klevstul

# https://www.gimp.org/
# https://search.nixos.org/packages?channel=23.11&from=0&size=50&sort=relevance&type=packages&query=gimp

{ pkgs, lib, osConfig, ... }:
let
    inherit (import ../../../hosts/${osConfig.networking.hostName}/options.nix) _gimp;
in
lib.mkIf (_gimp == true) {

    home.packages = with pkgs; [
        gimp-with-plugins
    ];

}
