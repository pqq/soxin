# klevstul

# https://git-scm.com/

{ lib, osConfig, ... }:
let
    inherit (import ../../../hosts/${osConfig.networking.hostName}/options.nix) _git;
in
{

    programs.git.enable = lib.mkIf _git true;

}
