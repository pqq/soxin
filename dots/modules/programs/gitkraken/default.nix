# klevstul

# https://www.gitkraken.com/

{ pkgs, lib, osConfig, ... }:
let
    inherit (import ../../../hosts/${osConfig.networking.hostName}/options.nix) _gitkraken;
in
lib.mkIf (_gitkraken == true) {

    home.packages = with pkgs; [
        gitkraken
    ];

}
