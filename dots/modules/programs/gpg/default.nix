# klevstul

# https://gnupg.org/
# https://discourse.nixos.org/t/cant-get-gnupg-to-work-no-pinentry/15373/32

{ lib, pkgs, osConfig, ... }:
let
    inherit (import ../../../hosts/${osConfig.networking.hostName}/options.nix) _gpg;
in
lib.mkIf (_gpg == true) {

    services.gnome-keyring.enable = true;
    programs.gpg.enable = true;
    services.gpg-agent = {
        enable = true;
        pinentryFlavor = "gnome3";
    };

}
