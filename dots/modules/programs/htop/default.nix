# klevstul

# https://htop.dev/

{ lib, osConfig, ... }:
let
    inherit (import ../../../hosts/${osConfig.networking.hostName}/options.nix) _htop;
in
{

    programs.htop.enable = lib.mkIf _htop true;

}
