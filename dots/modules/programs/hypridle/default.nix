# klevstul

# https://github.com/hyprwm/hypridle

{ pkgs, lib, inputs, osConfig, ... }:
let
    inherit (import ../../../hosts/${osConfig.networking.hostName}/options.nix) _hyprland;
in
lib.mkIf (_hyprland == true) {

    home.packages = with pkgs; [
        inputs.nixpkgs-unstable.legacyPackages.${pkgs.system}.hypridle
    ];

    home.file = {
        ".config/hypr/hypridle.conf" = {
            source = ./hypridle.conf;
        };
    };

}
