# klevstul

# https://wiki.hyprland.org/

{ pkgs, lib, inputs, osConfig, ... }:
let
    inherit (import ../../../hosts/${osConfig.networking.hostName}/options.nix) _hyprland;
in
lib.mkIf (_hyprland == true) {

    wayland.windowManager.hyprland.enable = true;
    wayland.windowManager.hyprland.xwayland.enable = true;
    wayland.windowManager.hyprland.extraConfig = (builtins.readFile ./hyprland.cfg);

    home.sessionVariables = {

        HELLO_WORLD_HOME_SV = "home.sessionVariables";

        # hint electron apps to use wayland
        NIXOS_OZONE_WL = "1";

    };

}
