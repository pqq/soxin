# klevstul

# https://wiki.hyprland.org/Hypr-Ecosystem/hyprlock/

{ pkgs, lib, inputs, osConfig, ... }:
let
    inherit (import ../../../hosts/${osConfig.networking.hostName}/options.nix) _hyprland;
in
lib.mkIf (_hyprland == true) {

    home.packages = [
        inputs.nixpkgs-unstable.legacyPackages.${pkgs.system}.hyprlock
    ];

    home.file = {
        ".config/hypr/hyprlock.conf" = {
            source = ./hyprlock.conf;
        };
    };

}
