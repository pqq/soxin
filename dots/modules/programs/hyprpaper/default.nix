# klevstul

# https://github.com/hyprwm/hyprpaper

{ pkgs, lib, osConfig, ... }:
let
    inherit (import ../../../hosts/${osConfig.networking.hostName}/options.nix) _hyprland;
in
lib.mkIf (_hyprland == true) {

    home.packages = with pkgs; [
        hyprpaper
    ];

    home.file = {
        ".config/hypr/hyprpaper.conf" = {
            source = ./hyprpaper.conf;
        };
    };

}
