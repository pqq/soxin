# klevstul

# https://github.com/Gustash/Hyprshot

{ pkgs, lib, inputs, osConfig, ... }:
let
    inherit (import ../../../hosts/${osConfig.networking.hostName}/options.nix) _hyprland;
in
lib.mkIf (_hyprland == true) {

    # overlay use is overkill. for details, see:
    # https://discourse.nixos.org/t/unstable-channel-undefined-variable-unstable/42130
    #home.packages = with pkgs; [
    #    unstable.hyprshot
    #];

    home.packages = [
        inputs.nixpkgs-unstable.legacyPackages.${pkgs.system}.hyprshot
    ];

     # note: a complete logout/login (alt. a reboot) is needed for this to take effect!
   home.sessionVariables = {
        HYPRSHOT_DIR = "Downloads";
    };

}
