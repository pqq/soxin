# klevstul

# https://github.com/gsamokovarov/jump

{ pkgs, lib, osConfig, ... }:
let
    inherit (import ../../../hosts/${osConfig.networking.hostName}/options.nix) _jump;
in
lib.mkIf (_jump == true) {

    home.packages = with pkgs; [
        jump
    ];

}
