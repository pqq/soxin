# klevstul

# https://github.com/kovidgoyal/kitty

{ lib, osConfig, ... }:
let
    inherit (import ../../../hosts/${osConfig.networking.hostName}/options.nix) _kitty;
in
{

    programs.kitty.enable = lib.mkIf _kitty true;

}
