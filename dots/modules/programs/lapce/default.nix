# klevstul

# https://github.com/lapce/lapce

{ pkgs, lib, osConfig, ... }:
let
    inherit (import ../../../hosts/${osConfig.networking.hostName}/options.nix) _lapce;
in
lib.mkIf (_lapce == true) {

    home.packages = with pkgs; [
        lapce
    ];

}
