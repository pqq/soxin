# klevstul

# https://home-manager-options.extranix.com/?query=lf&release=release-23.11
# https://github.com/gokcehan/lf
# https://github.com/gokcehan/lf/wiki/Tutorial

# two alternatives way of implementing the below code
# alternative 1, write the following directly after the "in":
# ```
#     lib.mkIf (_lf == true) {
#         programs.lf.enable = true;
#     }
# ```
# alternative 2, write the following, inside the last "{ }":
# ```
#     programs.lf.enable = lib.mkIf _lf true;
# ```

{ lib, osConfig, ... }:
let
    inherit (import ../../../hosts/${osConfig.networking.hostName}/options.nix) _lf;
in
lib.mkIf (_lf == true) {

    programs.lf.enable = true;

    programs.lf.extraConfig = ''

        # https://github.com/gokcehan/lf/blob/master/etc/lfrc.example

        # leave some space at the top and the bottom of the screen
        set scrolloff 10

        # dedicated keys for file opener actions
        map o &mimeopen $f
        map O $mimeopen --ask $f

        # define a custom 'open' command
        # This command is called when current file is not a directory. You may want to
        # use either file extensions and/or mime types here. Below uses an editor for
        # text files and a file opener for the rest.
        cmd open &{{
            case $(file --mime-type -Lb $f) in
                text/*) lf -remote "send $id \$$EDITOR \$fx";;
                *) for f in $fx; do $OPENER $f > /dev/null 2> /dev/null & done;;
            esac
        }}

        # mkdir command. See wiki if you want it to select created dir
        map a :push %mkdir<space>

        # define a custom 'rename' command without prompt for overwrite
        # cmd rename %[ -e $1 ] && printf "file exists" || mv $f $1
        # map r push :rename<space>

        # move current file or selected files to trash folder
        # (also see 'man mv' for backup/overwrite options)
        #cmd trash %set -f; mv $fx ~/.trash
        # $f: single file, $fx: multiple files
        cmd trash %trash-put $fx

        # use '<delete>' key for 'trash'
        map <delete> trash

        # extract the current file with the right command
        # (xkcd link: https://xkcd.com/1168/)
        cmd extract ''${{
            set -f
            case $f in
                *.tar.bz|*.tar.bz2|*.tbz|*.tbz2) tar xjvf $f;;
                *.tar.gz|*.tgz) tar xzvf $f;;
                *.tar.xz|*.txz) tar xJvf $f;;
                *.zip) unzip $f;;
                *.rar) unrar x $f;;
                *.7z) 7z x $f;;
            esac
        }}

        # compress current file or selected files with tar and gunzip
        cmd tar ''${{
            set -f
            mkdir $1
            cp -r $fx $1
            tar czf $1.tar.gz $1
            rm -rf $1
        }}

        # compress current file or selected files with zip
        cmd zip ''${{
            set -f
            mkdir $1
            cp -r $fx $1
            zip -r $1.zip $1
            rm -rf $1
        }}

    '';

}
