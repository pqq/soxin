# klevstul

# https://github.com/emersion/mako

{ pkgs, lib, osConfig, ... }:
let
    inherit (import ../../../hosts/${osConfig.networking.hostName}/options.nix) _hyprland;
in
lib.mkIf (_hyprland == true) {

    home.packages = with pkgs; [
        mako
    ];

    services.mako.enable = true;

}
