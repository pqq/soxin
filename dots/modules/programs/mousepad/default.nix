# klevstul

# https://gitlab.xfce.org/apps/mousepad

{ pkgs, lib, osConfig, ... }:
let
    inherit (import ../../../hosts/${osConfig.networking.hostName}/options.nix) _mousepad;
in
lib.mkIf (_mousepad == true) {

    home.packages = with pkgs; [
        xfce.mousepad
    ];

}
