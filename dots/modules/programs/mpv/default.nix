# klevstul

# https://mpv.io/

{ pkgs, lib, osConfig, ... }:
let
    inherit (import ../../../hosts/${osConfig.networking.hostName}/options.nix) _mpv;
in
lib.mkIf (_mpv == true) {

    programs.mpv.enable = true;

}
