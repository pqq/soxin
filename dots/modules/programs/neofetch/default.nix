# klevstul

{ pkgs, lib, osConfig, ... }:
let
    inherit (import ../../../hosts/${osConfig.networking.hostName}/options.nix) _neofetch;
in
lib.mkIf (_neofetch == true) {

    home.packages = with pkgs; [
        neofetch
    ];

    # did not work right away. unsure if i will bother to change it. however, the method done in
    # `dots/modules/programs/hypridle/default.nix` might work.
    # src: https://gitlab.com/Zaney/zaneyos/-/blob/main/config/home/neofetch.nix?ref_type=heads
    #home.file."~/.config/neofetch/config.conf".text = ''
    #    print_info() {
    #        prin "$(color 6)  KLEVSTUL $ZANEYOS_VERSION"
    #        info underline
    #        info "$(color 7)  VER" kernel
    #        info "$(color 2)  UP " uptime
    #        info "$(color 4)  PKG" packages
    #        info "$(color 6)  DE " de
    #        info "$(color 5)  TER" term
    #        info "$(color 3)  CPU" cpu
    #        info "$(color 7)  GPU" gpu
    #        info "$(color 5)  MEM" memory
    #        prin " "
    #        prin "$(color 1) $(color 2) $(color 3) $(color 4) $(color 5) $(color 6) $(color 7) $(color 8)"
    #    }
    #    distro_shorthand="on"
    #    memory_unit="gib"
    #    cpu_temp="C"
    #    separator=" $(color 4)>"
    #    stdout="off"
    #'';

}
