# klevstul

# https://home-manager-options.extranix.com/?query=neovim&release=release-23.11

{ lib, osConfig, ... }:
let
    inherit (import ../../../hosts/${osConfig.networking.hostName}/options.nix) _neovim;
in
lib.mkIf (_neovim == true) {

    programs.neovim.enable = true;

}
