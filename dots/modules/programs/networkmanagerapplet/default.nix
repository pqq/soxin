# klevstul

# https://gitlab.gnome.org/GNOME/network-manager-applet/

{ pkgs, lib, osConfig, ... }:
let
    inherit (import ../../../hosts/${osConfig.networking.hostName}/options.nix) _networkmanagerapplet;
in
lib.mkIf (_networkmanagerapplet == true) {

    home.packages = with pkgs; [
        networkmanagerapplet
    ];

}
