# klevstul

# https://nextcloud.com/

{ lib, pkgs, osConfig, ... }:
let
    inherit (import ../../../hosts/${osConfig.networking.hostName}/options.nix) _nextcloud-client;
in
lib.mkIf (_nextcloud-client == true) {

    #services.nextcloud-client.enable = true;
    #services.nextcloud-client.startInBackground = false;

    home.packages = with pkgs; [
        nextcloud-client
    ];

}
