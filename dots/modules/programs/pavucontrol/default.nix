# klevstul

{ pkgs, lib, osConfig, ... }:
let
    inherit (import ../../../hosts/${osConfig.networking.hostName}/options.nix) _pavucontrol;
in
lib.mkIf (_pavucontrol == true) {

    home.packages = with pkgs; [
        pavucontrol
    ];

}
