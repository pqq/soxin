# klevstul

# https://discourse.nixos.org/t/pcloud-gives-segmentation-fault/31330
# https://github.com/NixOS/nixpkgs/issues/226339
# https://gist.github.com/zarelit/c71518fe1272703788d3b5f570ef12e9

{ lib, pkgs, osConfig, ... }:
let
    inherit (import ../../../hosts/${osConfig.networking.hostName}/options.nix) _pcloud;

    patchelfFixes = pkgs.patchelfUnstable.overrideAttrs (_finalAttrs: _previousAttrs: {
            src = pkgs.fetchFromGitHub {
                owner = "Patryk27";
                repo = "patchelf";
                rev = "527926dd9d7f1468aa12f56afe6dcc976941fedb";
                sha256 = "sha256-3I089F2kgGMidR4hntxz5CKzZh5xoiUwUsUwLFUEXqE=";
            };
        });
        pcloudFixes = pkgs.pcloud.overrideAttrs (_finalAttrs:previousAttrs: {
            nativeBuildInputs = previousAttrs.nativeBuildInputs ++ [ patchelfFixes ];
        }
    );
in
lib.mkIf (_pcloud == true) {

    home.packages = with pkgs; [
        pcloudFixes
    ];

}
