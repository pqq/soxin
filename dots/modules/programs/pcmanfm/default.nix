# klevstul

{ pkgs, lib, osConfig, ... }:
let
    inherit (import ../../../hosts/${osConfig.networking.hostName}/options.nix) _pcmanfm;
in
lib.mkIf (_pcmanfm == true) {

    home.packages = with pkgs; [
        pcmanfm
    ];

}
