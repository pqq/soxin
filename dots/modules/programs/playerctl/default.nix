# klevstul

# https://github.com/altdesktop/playerctl

{ pkgs, lib, osConfig, ... }:
let
    inherit (import ../../../hosts/${osConfig.networking.hostName}/options.nix) _playerctl;
in
lib.mkIf (_playerctl == true) {

    home.packages = with pkgs; [
        playerctl
    ];

}
