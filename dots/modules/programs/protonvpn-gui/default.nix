# klevstul

# https://github.com/ProtonVPN/linux-app
# https://protonvpn.com/support/linux-vpn-setup/
# https://protonvpn.com/support/wireguard-manual-linux/

{ pkgs, lib, inputs, osConfig, ... }:
let
    inherit (import ../../../hosts/${osConfig.networking.hostName}/options.nix) _protonvpn;
in
lib.mkIf (_protonvpn == true) {

    home.packages = with pkgs; [
        protonvpn-gui
        #inputs.nixpkgs-unstable.legacyPackages.${pkgs.system}.protonvpn-gui
    ];

}
