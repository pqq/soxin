# klevstul

# https://github.com/altdesktop/playerctl

{ pkgs, lib, osConfig, ... }:
let
    inherit (import ../../../hosts/${osConfig.networking.hostName}/options.nix) _python;
in
lib.mkIf (_python == true) {

    home.packages = with pkgs; [
        python3
    ];

}
