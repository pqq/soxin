# klevstul

# transferring reaper settings
#   https://forum.cockos.com/showthread.php?t=158152

{ lib, pkgs, osConfig, ... }:
let
    inherit (import ../../../hosts/${osConfig.networking.hostName}/options.nix) _reaper;
in
lib.mkIf (_reaper == true) {

    home.packages = with pkgs; [
        reaper
    ];

    home.file = {
        ".misc/reaper.ReaperConfigZip" = {
            source = ./reaper.ReaperConfigZip;
        };
    };

}
