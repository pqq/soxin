# klevstul

# https://shotcut.org/

{ lib, pkgs, osConfig, ... }:
let
    inherit (import ../../../hosts/${osConfig.networking.hostName}/options.nix) _shotcut;
in
lib.mkIf (_shotcut == true) {

    home.packages = with pkgs; [
        shotcut
    ];

    home.file = {
        ".local/share/Meltytech/Shotcut/filter-sets" = {
            source = ./filter-sets;
            recursive = true;
        };
    };

}
