# klevstul

# https://www.stremio.com/

{ pkgs, lib, osConfig, ... }:
let
    inherit (import ../../../hosts/${osConfig.networking.hostName}/options.nix) _stremio;
in
lib.mkIf (_stremio == true) {

    home.packages = with pkgs; [
        stremio
    ];

}
