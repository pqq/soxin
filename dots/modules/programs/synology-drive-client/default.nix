# klevstul

# https://github.com/nwg-piotr/nwg-drawer

{ pkgs, lib, osConfig, ... }:
let
    inherit (import ../../../hosts/${osConfig.networking.hostName}/options.nix) _synology-drive-client;
in
lib.mkIf (_synology-drive-client == true) {

    home.packages = with pkgs; [
        synology-drive-client
    ];

}
