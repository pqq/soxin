# klevstul

# https://tmux.github.io/

{ pkgs, lib, osConfig, ... }:
let
    inherit (import ../../../hosts/${osConfig.networking.hostName}/options.nix) _tmux;
in
{

    programs.tmux.enable = lib.mkIf _tmux true;

}
