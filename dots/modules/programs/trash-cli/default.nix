# klevstul

# https://github.com/andreafrancia/trash-cli

{ pkgs, lib, osConfig, ... }:
let
    inherit (import ../../../hosts/${osConfig.networking.hostName}/options.nix) _trash-cli;
in
lib.mkIf (_trash-cli == true) {

    home.packages = with pkgs; [
        trash-cli
    ];

}
