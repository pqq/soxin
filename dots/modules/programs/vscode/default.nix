# klevstul

# https://home-manager-options.extranix.com/?query=vscode&release=master

{ lib, pkgs, osConfig, ... }:
let
    inherit (import ../../../hosts/${osConfig.networking.hostName}/options.nix) _vscode;
in
lib.mkIf (_vscode == true) {

    programs.vscode.enable = true;
    programs.vscode.package = pkgs.vscodium;
    programs.vscode.userSettings = {
        "trailing-spaces.trimOnSave" = true;
        "trailing-spaces.syntaxIgnore" = [
            "md"
            "markdn"
            "markdown"
        ];
        "workbench.colorTheme" = "Night Owl (No Italics)";
        "workbench.iconTheme" = null;
        "editor.lightbulb.enabled" = "off";
        "editor.matchBrackets" = "never";
        "editor.hover.above" = false;
        "editor.hover.delay" = 3500;
        "editor.detectIndentation" = false;
        "typescript.disableAutomaticTypeAcquisition" = true;
        "editor.hover.enabled" = false;
        "editor.parameterHints" = false;
        "workbench.editor.wrapTabs" = true;
        "editor.rulers" = [
            125
        ];
        "explorer.autoReveal" = false;
        "git.confirmSync" = false;
    };

}
