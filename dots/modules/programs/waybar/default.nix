# klevstul

# https://github.com/Alexays/Waybar

{ lib, osConfig, ... }:
let
    inherit (import ../../../hosts/${osConfig.networking.hostName}/options.nix) _hyprland;
in
lib.mkIf (_hyprland == true) {

    programs.waybar.enable = true;

    # auto start waybar
    # note: started in hyprland.cond
    #programs.waybar.systemd.enable = true;
    #programs.waybar.systemd.target = "graphical-session.target";

    # info on how to read a config file instead of specifying setting in nix:
    # https://discourse.nixos.org/t/builtins-readfile-to-read-a-config-file-for-waybar/42025/

    programs.waybar.settings = [{

        layer = "top";
        height = 30;

        modules-left    = [
            "hyprland/workspaces"
            "tray"
        ];
        modules-center  = [
            #"hyprland/window"   # display title of focused window
        ];
        modules-right   = [
            "idle_inhibitor"
            "pulseaudio"
            "network"
            "memory"
            "disk"
            "cpu"
            "battery"
            "temperature"
            "clock"
        ];

        # https://github.com/Alexays/Waybar/wiki/Module:-Battery
        "battery"= {
            "format"        = "{capacity}% {icon}";
            "format-icons"  = ["" "" "" "" ""];
        };

        # https://github.com/Alexays/Waybar/wiki/Module:-Clock
        "clock"= {
            "format"         = "{:%H:%M}  ";
            "format-alt"     = "{:%A, %B %d, %Y (%R)}  ";
            "tooltip-format" = "<tt><small>{calendar}</small></tt>";
            "calendar" = {
                        "mode"           = "year";
                        "mode-mon-col"   = 3;
                        "weeks-pos"      = "right";
                        "on-scroll"      = 1;
                        "on-click-right" = "mode";
                        "format"= {
                                "months"   = "<span color='#ffead3'><b>{}</b></span>";
                                "days"     = "<span color='#ecc6d9'><b>{}</b></span>";
                                "weeks"    = "<span color='#99ffdd'><b>W{}</b></span>";
                                "weekdays" = "<span color='#ffcc66'><b>{}</b></span>";
                                "today"    = "<span color='#ff6699'><b><u>{}</u></b></span>";
                                };
                        };
            "actions"=  {
                        "on-click-right"    = "mode";
                        "on-click-forward"  = "tz_up";
                        "on-click-backward" = "tz_down";
                        "on-scroll-up"      = "shift_up";
                        "on-scroll-down"    = "shift_down";
                        };
        };

        # https://wiki.hyprland.org/Useful-Utilities/Status-Bars/#window-title-is-missing
        # https://manpages.opensuse.org/Tumbleweed/waybar/waybar-hyprland-window.5.en.html
        "hyprland/window" = {
            "max-length" = 200;
            "separate-outputs" = true;
        };

        # https://github.com/Alexays/Waybar/wiki/Module:-Workspaces
        # https://github.com/Alexays/Waybar/wiki/Module:-Hyprland
        "hyprland/workspaces" = {
            format = "{icon}{name}";
            format-icons = {
                default = " ";
                active  = " .";
                urgent  = " !";
            };
            on-scroll-up   = "hyprctl dispatch workspace e+1";
            on-scroll-down = "hyprctl dispatch workspace e-1";
        };

        # The idle_inhibitor module can inhibit idle behavior such as screen blanking,
        # locking, and screensaving, also known as "presentation mode".
        # https://github.com/Alexays/Waybar/wiki/Module:-Idle-Inhibitor
        "idle_inhibitor" = {
            "format" = "{icon}";
            "format-icons" = {
                "activated"   = "";
                "deactivated" = "";
            };
        };

        # https://github.com/Alexays/Waybar/wiki/Module:-CPU
        "cpu" = {
            "interval" = 10;
            "format" = "{}% ";
            "max-length" = 10;
        };

        # https://github.com/Alexays/Waybar/wiki/Module:-Disk
        "disk" = {
            "interval" = 30;
            "format" = "{path}:{used}/{total} ({percentage_used}%)";
            "path" = "/";
        };

        # https://github.com/Alexays/Waybar/wiki/Module:-Memory
        "memory" = {
            "interval"   = 30;
            "format"     = "{used:0.1f}/{total:0.1f}G ({}%) ";
        };

        # https://github.com/Alexays/Waybar/wiki/Module:-Network
        "network" = {
            #"interface" = "wlp2s0";
            "format" = "{ifname}";
            "format-wifi" = "{essid} ({signalStrength}%) ";
            "format-ethernet" = "{ipaddr}/{cidr} 󰊗";
            "format-disconnected" = " "; # An empty format will hide the module.
            "tooltip-format" = "{ifname} via {gwaddr} 󰊗";
            "tooltip-format-wifi" = "{essid} ({signalStrength}%) ";
            "tooltip-format-ethernet" = "{ifname} ";
            "tooltip-format-disconnected" = "Disconnected";
            "max-length" = 50;
            "on-click" = "nm-applet";
        };

        # https://github.com/Alexays/Waybar/wiki/Module:-PulseAudio
        "pulseaudio" = {
            "format" = "{volume}% {icon}";
            "format-bluetooth" = "{volume}% {icon}";
            "format-muted" = "";
            "format-icons" = {
                "headphone"  = "";
                "hands-free" = "";
                "headset"    = "";
                "phone"      = "";
                "portable"   = "";
                "car"        = "";
                "default"    = ["" ""];
            };
            "scroll-step"   = 1;
            "on-click"      = "pavucontrol";
            "ignored-sinks" = ["Easy Effects Sink"];
        };

        # https://github.com/Alexays/Waybar/wiki/Module:-Temperature
        "temperature" = {
            # "thermal-zone" = 2;
            # "hwmon-path" = "/sys/class/hwmon/hwmon2/temp1_input";
            # "critical-threshold" = 80;
            # "format-critical" = "{temperatureC}°C ";
            "format" = "{temperatureC}°C ";
        };

    }];

    # waybar styling
    programs.waybar.style = (builtins.readFile ./styles.css);

}
