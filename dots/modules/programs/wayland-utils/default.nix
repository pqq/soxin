# klevstul

# https://gitlab.freedesktop.org/wayland/wayland-utils

{ pkgs, lib, osConfig, ... }:
let
    inherit (import ../../../hosts/${osConfig.networking.hostName}/options.nix) _hyprland;
in
lib.mkIf (_hyprland == true) {

    home.packages = with pkgs; [
        wayland-utils
    ];

}
