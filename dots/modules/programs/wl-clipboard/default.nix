# klevstul

# https://github.com/bugaevc/wl-clipboard

{ pkgs, lib, osConfig, ... }:
let
    inherit (import ../../../hosts/${osConfig.networking.hostName}/options.nix) _hyprland;
in
lib.mkIf (_hyprland == true) {

    home.packages = with pkgs; [
        wl-clipboard
    ];

}
