# klevstul

# https://sr.ht/%7Eemersion/wlr-randr/

{ pkgs, lib, osConfig, ... }:
let
    inherit (import ../../../hosts/${osConfig.networking.hostName}/options.nix) _wlr-randr;
in
lib.mkIf (_wlr-randr == true) {

    home.packages = with pkgs; [
        wlr-randr
    ];

}
