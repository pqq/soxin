# klevstul

# https://www.freedesktop.org/wiki/Software/xdg-utils/

{ pkgs, lib, osConfig, ... }:
let
    inherit (import ../../../hosts/${osConfig.networking.hostName}/options.nix) _xdg-utils;
in
lib.mkIf (_xdg-utils == true) {

    home.packages = with pkgs; [
        xdg-utils
    ];

}
