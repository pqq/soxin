# klevstul

# https://github.com/blueman-project/blueman - gtk-based bluetooth manager

{ lib, config, ... }:
let
    inherit (import ../../../hosts/${config.networking.hostName}/options.nix) _bluetooth _bluetooth_powerOnBoot;
in
{

    hardware.bluetooth.enable = lib.mkIf _bluetooth true;
    hardware.bluetooth.powerOnBoot = lib.mkIf _bluetooth_powerOnBoot true;
    services.blueman.enable = lib.mkIf _bluetooth true;

}
