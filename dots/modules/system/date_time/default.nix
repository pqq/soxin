# klevstul

{ ... }:
{

    # https://en.wikipedia.org/wiki/List_of_tz_database_time_zones
    time.timeZone = "America/Panama";

    # automatically update time zone
    # https://discourse.nixos.org/t/timezones-how-to-setup-on-a-laptop/33853/4
    #services.automatic-timezoned.enable = true;

}
