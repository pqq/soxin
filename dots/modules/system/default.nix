# system module :: klevstul

{ ... }:
{

    imports = [

        ./agc               # automatic garbage collection
        ./bluetooth
        ./bootloader
        ./date_time
        ./environment       # environment variables
        ./flakes            # enable flakes
        ./locales           # locales / internationalisation
        ./nixpkgs           # nixpkgs overlays & configuration
        ./nvidia
        ./opengl
        ./sound
        ./systemPackages    # base / system packages (inc scripts)
        ./tuxedo-rs         # tuxedo utility
        ./users

    ];

}
