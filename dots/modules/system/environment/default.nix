# klevstul

{ ... }:
{

    # ----------
    # system environment variables
    #
    # wayland specific user envs are set in "dots/packages/hyprland/hyprland.nix"
    # other user specific user env, see packages
    # ----------

    # please note that a reboot or complete logout/login might be needed for this to take effect
    environment.sessionVariables = {

        HELLO_WORLD_ENV_SV = "environment.sessionVariables";

    };

}
