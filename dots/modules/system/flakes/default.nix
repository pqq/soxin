# klevstul

{ config, lib, inputs, ... }:
{

    # enable flakes
    nix = {
        # This will add each flake input as a registry
        # To make nix3 commands consistent with your flake
        # Inspired by https://github.com/Misterio77/nix-starter-configs/blob/972935c1b35d8b92476e26b0e63a044d191d49c3/standard/nixos/configuration.nix
        # Misterio77:
        #   nix.registry = (lib.mapAttrs (_: flake: {inherit flake;})) ((lib.filterAttrs (_: lib.isType "flake")) inputs);
        registry = lib.mapAttrs (_: value: { flake = value; }) inputs;

        # This will additionally add your inputs to the system's legacy channels
        # Making legacy nix commands consistent as well, awesome!
        # Misterio77:
        #   nix.nixPath = ["/etc/nix/path"];
        #   environment.etc =
        #     lib.mapAttrs'
        #     (name: value: {
        #       name = "nix/path/${name}";
        #       value.source = value.flake;
        #     })
        #   config.nix.registry;
        nixPath = lib.mapAttrsToList (key: value: "${key}=${value.to.path}") config.nix.registry;

        settings = {
            # Enable flakes and new 'nix' command
            experimental-features = "nix-command flakes";

            # Deduplicate and optimize nix store
            auto-optimise-store = true;
        };
    };

}
