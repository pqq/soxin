# klevstul

# `$ locale`
# LC_ALL: https://unix.stackexchange.com/questions/87745/what-does-lc-all-c-do

{ config, ... }:
let
    inherit (import ../../../hosts/${config.networking.hostName}/options.nix) _keymap _locale;
in
{

    # ----------
    # locales / internationalisation properties
    # ----------
    i18n.defaultLocale = _locale;
    i18n.extraLocaleSettings = {
        LC_ADDRESS          = _locale;
        LC_IDENTIFICATION   = _locale;
        LC_MEASUREMENT      = _locale;
        LC_MONETARY         = _locale;
        LC_NAME             = _locale;
        LC_NUMERIC          = _locale;
        LC_PAPER            = _locale;
        LC_TELEPHONE        = _locale;
        LC_TIME             = _locale;
    };

    # Configure console keymap
    console.keyMap = _keymap;

}
