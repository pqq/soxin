# klevstul

{ inputs, ... }:
{

    # ----------
    # nixpkgs
    # ----------
    nixpkgs = {

        # overlays
        overlays = [
            # the following overlay is not needed. for details, please see:
            # https://discourse.nixos.org/t/unstable-channel-undefined-variable-unstable/42130
            #inputs.self.outputs.unstable-packages
        ];

        # configure the nixpkgs instance
        config = {
            allowUnfree = true;
        };

    };

}
