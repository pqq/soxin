# klevstul

{ lib, config, ... }:
let
    inherit (import ../../../hosts/${config.networking.hostName}/options.nix) _nvidia _nvidia_intelBusId _nvidia_nvidiaBusId;
in
lib.mkIf (_nvidia == true) {

    # load nvidia driver for wayland (and xorg)
    services.xserver.videoDrivers = ["nvidia"];

    # nvidia settings
    hardware.nvidia = {

        # Modesetting is required.
        modesetting.enable = true;

        powerManagement = {
                # Nvidia power management. Experimental, and can cause sleep/suspend to fail.
                enable = true; # must be false if prime's sync mode is used

                # Fine-grained power management. Turns off GPU when not in use.
                # Experimental and only works on modern Nvidia GPUs (Turing or newer).
                finegrained = true;
        };

        # Use the NVidia open source kernel module (not to be confused with the
        # independent third-party "nouveau" open source driver).
        # Support is limited to the Turing and later architectures. Full list of
        # supported GPUs is at:
        # https://github.com/NVIDIA/open-gpu-kernel-modules#compatible-gpus
        # Only available from driver 515.43.04+
        # Currently alpha-quality/buggy, so false is currently the recommended setting.
        open = false;

        # Optionally, you may need to select the appropriate driver version for your specific GPU.
        package = config.boot.kernelPackages.nvidiaPackages.stable;

        prime = {
            # Optimus PRIME Option A: Offload Mode
		    offload = {
		    	enable = true;
	    		enableOffloadCmd = true;
    		};

            # Optimus PRIME Option B: Sync Mode
            #sync.enable = true;

            # lshw -c display
            intelBusId = _nvidia_intelBusId;
            nvidiaBusId = _nvidia_nvidiaBusId;
        };

        # Enable the Nvidia settings menu,
	    # accessible via `nvidia-settings`.
        nvidiaSettings = true;

    };

}
