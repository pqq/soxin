# klevstul

# https://search.nixos.org/options?channel=23.11&show=hardware.opengl.extraPackages32&from=0&size=50&sort=relevance&type=packages&query=hardware.opengl

{ lib, config, ... }:
let
    inherit (import ../../../hosts/${config.networking.hostName}/options.nix) _opengl;
in
lib.mkIf (_opengl == true) {

    hardware.opengl = {
        enable = true;
        driSupport = true;
        driSupport32Bit = true;
    };

}
