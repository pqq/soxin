# klevstul

{ pkgs, ... }:
{

    # ----------
    # package & script installation
    # ----------
    environment.systemPackages = with pkgs; [
        nano    # cli editor
        vim     # cli editor
        wget    # cli downloader

        (pkgs.writeShellScriptBin "curae" ''
            script="/home/.curae/curae.sh"
            echo "$script $1 $2"
            $script "$1" "$2"
        '')

        (pkgs.writeShellScriptBin "ur" ''
            script="/home/.curae/curae.sh ur"
            echo "$script $1 $2"
            $script "$1" "$2"
        '')

    ];

}
