# klevstul

# https://github.com/AaronErhardt/tuxedo-rs

{ lib, config, ... }:
let
    inherit (import ../../../hosts/${config.networking.hostName}/options.nix) _tuxedo-rs;
in
{

    hardware.tuxedo-rs.tailor-gui.enable = lib.mkIf _tuxedo-rs true;
    hardware.tuxedo-rs.enable = lib.mkIf _tuxedo-rs true;

}
