# klevstul

# hashedPassword:
# generated using `mkpasswd`
# ref: https://nixos.org/manual/nixos/stable/options#opt-users.users._name_.hashedPassword

{ lib, config, ... }:
let
    inherit (import ../../../../hosts/${config.networking.hostName}/options.nix) _users;
in
lib.mkIf (builtins.elem "poq" _users) {

    users.users.poq = {
        hashedPassword="$y$j9T$uxkPmP1siC.Ip4T8ODLYs0$XtcGWUU4cERZPg7uU.MaJL0TkSkUpvG9oK3YdmrWe38";
        isNormalUser = true;
        extraGroups = [ "networkmanager" "wheel" ];
    };

}
