{
    description = "pqq/dob's nixos flake";

    # This is the standard format for flake.nix.
    # `inputs` are the dependencies of the flake,
    # and `outputs` function will return all the build results of the flake.
    # Each item in `inputs` will be passed as a parameter to
    # the `outputs` function after being pulled and built.

    inputs = {
        # There are many ways to reference flake inputs.
        # The most widely used is `github:owner/name/reference`,
        # which represents the GitHub repository URL + branch/commit-id/tag.

        # example of getting using a specific commit hash
        # https://discourse.nixos.org/t/cant-update-nvidia-driver-on-stable-branch/39246/17?u=dob
        # nixpkgs_patched.url = "github:nixos/nixpkgs/468a37e6ba01c45c91460580f345d48ecdb5a4db";

        # Official NixOS package source, using nixos-23.11 branch here
        nixpkgs.url = "github:NixOS/nixpkgs/nixos-23.11";
        #nixpkgs.url = "github:NixOS/nixpkgs/nixos-unstable";

        nixpkgs-unstable.url = "github:nixos/nixpkgs/nixos-unstable";

        # home-manager, used for managing user configuration
        home-manager = {
            url = "github:nix-community/home-manager/release-23.11";
            # The `follows` keyword in inputs is used for inheritance.
            # Here, `inputs.nixpkgs` of home-manager is kept consistent with
            # the `inputs.nixpkgs` of the current flake,
            # to avoid problems caused by different versions of nixpkgs.
            inputs.nixpkgs.follows = "nixpkgs";
        };

        # https://nixos-and-flakes.thiscute.world/nixos-with-flakes/nixos-with-flakes-enabled
        #helix.url = "github:helix-editor/helix/master";

    };




#  outputs = {
#    self,
#    nixpkgs,
#    home-manager,
#    ...
#  } @ inputs: let
#    inherit (self) outputs;
#    # Supported systems for your flake packages, shell, etc.
#    systems = [
#      "aarch64-linux"
#      "x86_64-linux"
#    ];
#    # This is a function that generates an attribute by calling a function you
#    # pass to it, with each system as an argument
#    forAllSystems = nixpkgs.lib.genAttrs systems;
#  in {
#    # Your custom packages
#    # Accessible through 'nix build', 'nix shell', etc
#    packages = forAllSystems (system: import ./pkgs nixpkgs.legacyPackages.${system});
#    # Formatter for your nix files, available through 'nix fmt'
#    # Other options beside 'alejandra' include 'nixpkgs-fmt'
#    formatter = forAllSystems (system: nixpkgs.legacyPackages.${system}.alejandra);
#
#    # Your custom packages and modifications, exported as overlays
#    overlays = import ./overlays {inherit inputs;};
#    # Reusable nixos modules you might want to export
#    # These are usually stuff you would upstream into nixpkgs
#    nixosModules = import ./modules/nixos;
#    # Reusable home-manager modules you might want to export
#    # These are usually stuff you would upstream into home-manager
#    homeManagerModules = import ./modules/home-manager;
#
#    # NixOS configuration entrypoint
#    # Available through 'nixos-rebuild --flake .#your-hostname'
#    nixosConfigurations = {
#      # FIXME replace with your hostname
#      t470p = nixpkgs.lib.nixosSystem {
#        specialArgs = {inherit inputs outputs;};
#        modules = [
#          # > Our main nixos configuration file <
#          ./nixos/configuration.nix
#        ];
#      };
#    };
#
#    # Standalone home-manager configuration entrypoint
#    # Available through 'home-manager --flake .#your-username@your-hostname'
#    homeConfigurations = {
#      # FIXME replace with your username@hostname
#      "poq@t470p" = home-manager.lib.homeManagerConfiguration {
#        pkgs = nixpkgs.legacyPackages.x86_64-linux; # Home-manager requires 'pkgs' instance
#        extraSpecialArgs = {inherit inputs outputs;};
#        modules = [
#          # > Our main home-manager configuration file <
#          ./home-manager/home.nix
#        ];
#      };
#    };
#  };







    # `outputs` are all the build result of the flake.
    #
    # A flake can have many use cases and different types of outputs.
    #
    # parameters in function `outputs` are defined in `inputs` and
    # can be referenced by their names. However, `self` is an exception,
    # this special parameter points to the `outputs` itself(self-reference)
    #
    # The `@` syntax here is used to alias the attribute set of the
    # inputs's parameter, making it convenient to use inside the function.
    outputs = { self, nixpkgs, ... }@inputs: {
#    outputs = inputs@{ self, nixpkgs, ... }: {
        nixosConfigurations = {
            # By default, NixOS will try to refer the nixosConfiguration with
            # its hostname, so the system named `nixos-test` will use this one.
            # However, the configuration name can also be specified using:
            #   sudo nixos-rebuild switch --flake /path/to/flakes/directory#<name>
            #
            # The `nixpkgs.lib.nixosSystem` function is used to build this
            # configuration, the following attribute set is its parameter.
            #
            # Run the following command in the flake's directory to
            # deploy this configuration on any NixOS system:
            #   sudo nixos-rebuild switch --flake .#nixos-test
            "t470p" = nixpkgs.lib.nixosSystem {
                system = "x86_64-linux";

                # The Nix module system can modularize configuration,
                # improving the maintainability of configuration.
                #
                # Each parameter in the `modules` is a Nixpkgs Module, and
                # there is a partial introduction to it in the nixpkgs manual:
                #    <https://nixos.org/manual/nixpkgs/unstable/#module-system-introduction>
                # It is said to be partial because the documentation is not
                # complete, only some simple introductions.
                # such is the current state of Nix documentation...
                #
                # A Nixpkgs Module can be an attribute set, or a function that
                # returns an attribute set. By default, if a Nixpkgs Module is a
                # function, this function has the following default parameters:
                #
                #  lib:     the nixpkgs function library, which provides many
                #             useful functions for operating Nix expressions:
                #             https://nixos.org/manual/nixpkgs/stable/#id-1.4
                #  config:  all config options of the current flake, very useful
                #  options: all options defined in all NixOS Modules
                #             in the current flake
                #  pkgs:   a collection of all packages defined in nixpkgs,
                #            plus a set of functions related to packaging.
                #            you can assume its default value is
                #            `nixpkgs.legacyPackages."${system}"` for now.
                #            can be customed by `nixpkgs.pkgs` option
                #  modulesPath: the default path of nixpkgs's modules folder,
                #               used to import some extra modules from nixpkgs.
                #               this parameter is rarely used,
                #               you can ignore it for now.
                #
                # The default parameters mentioned above are automatically
                # generated by Nixpkgs.
                # However, if you need to pass other non-default parameters
                # to the submodules,
                # you'll have to manually configure these parameters using
                # `specialArgs`.
                # you must use `specialArgs` by uncommenting the following line:
                #
                # specialArgs = {...};  # pass custom arguments into all submodules.
                specialArgs = { inherit inputs; };
                #specialArgs = inputs;

                modules = [
                    # Import the configuration.nix here, so that the
                    # old configuration file can still take effect.
                    # Note: configuration.nix itself is also a Nixpkgs Module,
                    ./nixos/configuration.t470p.nix
                ];
            };
        };
    };
}