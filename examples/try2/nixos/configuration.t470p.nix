# Edit this configuration file to define what should be installed on
# your system. Help is available in the configuration.nix(5) man page
# and in the NixOS manual (accessible by running ‘nixos-help’).

{ config, pkgs, lib, inputs, ... }:

{
    imports =
    [   # Include the results of the hardware scan.
        ./hardware-configuration.t470p.nix
    ];




 	  ## Enable flakes
	  nix = {
	    # This will add each flake input as a registry
	    # To make nix3 commands consistent with your flake
	    # Inspired by https://github.com/Misterio77/nix-starter-configs/blob/972935c1b35d8b92476e26b0e63a044d191d49c3/standard/nixos/configuration.nix
	    registry = lib.mapAttrs (_: value: { flake = value; }) inputs;

	    # This will additionally add your inputs to the system's legacy channels
	    # Making legacy nix commands consistent as well, awesome!
	    nixPath = lib.mapAttrsToList (key: value: "${key}=${value.to.path}") config.nix.registry;

	    settings = {
	      # Enable flakes and new 'nix' command
	      experimental-features = "nix-command flakes";
	    };
	  };





    # Not really needed, but recommended if you want to run non-nixos binaries.
    # You can customize programs.nix-ld.libraries = []; to add more libraries not present in systemPackages.
    # https://gist.github.com/tobiasBora/1f19628dc6b12bcd4438164b24226c6c
	programs.nix-ld.enable = true;


    # Enable flakes
    # https://discourse.nixos.org/t/how-to-start-using-nix-os/37804/26?u=dob
    # This will add each flake input as a registry
    # To make nix3 commands consistent with your flake
    # Inspired by https://github.com/Misterio77/nix-starter-configs/blob/972935c1b35d8b92476e26b0e63a044d191d49c3/standard/nixos/configuration.nix
    #registry = lib.mapAttrs (_: value: { flake = value; }) inputs;

    # This will additionally add your inputs to the system's legacy channels
    # Making legacy nix commands consistent as well, awesome!
    #nixPath = lib.mapAttrsToList (key: value: "${key}=${value.to.path}") config.nix.registry;

    # Enable flakes permanently in NixOS, including the new command
    #nix.settings.experimental-features = [ "nix-command" "flakes" ];

    # Bootloader.
    boot.loader.systemd-boot.enable = true;
    boot.loader.efi.canTouchEfiVariables = true;
    #  boot.loader.grub.enable = true;
    #  boot.loader.grub.device = "/dev/sda";
    #  boot.loader.grub.useOSProber = true;

    networking.hostName = "nixos"; # Define your hostname.
    # networking.wireless.enable = true;  # Enables wireless support via wpa_supplicant.

    # Configure network proxy if necessary
    # networking.proxy.default = "http://user:password@proxy:port/";
    # networking.proxy.noProxy = "127.0.0.1,localhost,internal.domain";

    # Enable networking
    networking.networkmanager.enable = true;

    # Set your time zone.
    time.timeZone = "America/New_York";

    # Select internationalisation properties.
    i18n.defaultLocale = "en_DK.UTF-8";

    i18n.extraLocaleSettings = {
        LC_ADDRESS = "en_DK.UTF-8";
        LC_IDENTIFICATION = "en_DK.UTF-8";
        LC_MEASUREMENT = "en_DK.UTF-8";
        LC_MONETARY = "en_DK.UTF-8";
        LC_NAME = "en_DK.UTF-8";
        LC_NUMERIC = "en_DK.UTF-8";
        LC_PAPER = "en_DK.UTF-8";
        LC_TELEPHONE = "en_DK.UTF-8";
        LC_TIME = "en_DK.UTF-8";
    };

    # Enable the X11 windowing system.
    #services.xserver.enable = true;

    # Enable the XFCE Desktop Environment.
    #services.xserver.displayManager.lightdm.enable = true;
    #services.xserver.desktopManager.xfce.enable = true;

    # Configure keymap in X11
    #services.xserver = {
    #  layout = "us";
    #  xkbVariant = "";
    #};

    # Configure console keymap
    console.keyMap = "us";

    # Enable CUPS to print documents.
    #services.printing.enable = true;

    #services.avahi = {
    #  enable = true;
    #  nssmdns = true;
    #  openFirewall = true;
    #};

    # Enable sound with pipewire.
    sound.enable = true;
    hardware.pulseaudio.enable = false;
    security.rtkit.enable = true;
    services.pipewire = {
        enable = true;
        alsa.enable = true;
        alsa.support32Bit = true;
        pulse.enable = true;
        # If you want to use JACK applications, uncomment this
        #jack.enable = true;

        # use the example session manager (no others are packaged yet so this is enabled by default,
        # no need to redefine it in your config for now)
        #media-session.enable = true;
    };

    # Enable touchpad support (enabled default in most desktopManager).
    # services.xserver.libinput.enable = true;

    # Define a user account. Don't forget to set a password with ‘passwd’.
    users.users.poq = {
        isNormalUser = true;
        description = "";
        extraGroups = [ "networkmanager" "wheel" ];
        packages = with pkgs; [];
    };

    # Enable automatic login for the user.
    #services.xserver.displayManager.autoLogin.enable = true;
    #services.xserver.displayManager.autoLogin.user = "poq";

    # Allow unfree packages
    nixpkgs.config.allowUnfree = true;

    # List packages installed in system profile. To search, run:
    # $ nix search wget
    environment.systemPackages = with pkgs; [
        vim                         # editor
        wget                        # download from cli
#        waybar                      # hyprland status bar
#        mako                        # wayland notification daemon
        libnotify                   # library for sending desktop notifications to a notification daemon
#        rofi-wayland                # window switcher, app launcher and dmenu replacement
#        xwayland                    # x srv under wayland, provides compatibility for X11 apps without wayland support
#        swww                        # wayland wallpaper
#        meson                       # for automating the building (compiling) of software
#        wayland-protocols           # wayland protocol extensions
#        wayland-utils               # wayland utilities (wayland-info)
#        wl-clipboard                # cli copy/paste utilities for wayland
#        wlroots                     # a modular wayland compositor library
#        pavucontrol                 # pulseaudio volume control
#        pipewire                    # server and user space API to deal with multimedia pipelines
#        firefox                     # browser
        #brave                      # browser
        #ungoogled-chromium         # browser
        #mpv                        # media player
        git                         # distributed version control system
#        kitty                       # terminal (used by hyprland)
        neofetch                    # a fast, highly customizable system info script
        #vscodium                   # open source source code editor
        pciutils                    # programs for inspecting and manipulating config of PCI devices (lspci)
        #gnome.gdm                  # manages graphical display servers and handles graphical user logins
        lshw                        # provide detailed information on the hardware configuration of the machine
        inxi                        # cli system information tool
        #gamescope
        #seatd # needed by gamescope
        glxinfo                     # test utilities for opengl


        #home-manager.packages."${pkgs.system}".home-manager

        #inputs.helix.packages."${pkgs.system}".helix

        (pkgs.writeShellScriptBin "nvidia-offload" ''
            export __NV_PRIME_RENDER_OFFLOAD=1
            export __NV_PRIME_RENDER_OFFLOAD_PROVIDER=NVIDIA-G0
            export __GLX_VENDOR_LIBRARY_NAME=nvidia
            export __VK_LAYER_NV_optimus=NVIDIA_only
            exec "$0"
        '')

    ];

    # Some programs need SUID wrappers, can be configured further or are
    # started in user sessions.
    # programs.mtr.enable = true;
    # programs.gnupg.agent = {
    #   enable = true;
    #   enableSSHSupport = true;
    # };

    # List services that you want to enable:
    #services.qemuGuest.enable = true;
    #services.spice-vdagentd.enable = true;
    services.gnome.gnome-keyring.enable = true;
    # Enable the OpenSSH daemon.
    #services.openssh.enable = true;

    # Open ports in the firewall.
    # networking.firewall.allowedTCPPorts = [ ... ];
    # networking.firewall.allowedUDPPorts = [ ... ];
    # Or disable the firewall altogether.
    # networking.firewall.enable = false;

    # This value determines the NixOS release from which the default
    # settings for stateful data, like file locations and database versions
    # on your system were taken. It‘s perfectly fine and recommended to leave
    # this value at the release version of the first install of this system.
    # Before changing this value read the documentation for this option
    # (e.g. man configuration.nix or on https://nixos.org/nixos/options.html).
    system.stateVersion = "23.11"; # Did you read the comment?

    # ----------

    # OBS virt Webcam
    #boot.extraModulePackages = with config.boot.kernelPackages; [ v4l2loopback ];

    # Automatic Garbage Collection
    nix.gc = {
        automatic = true;
        dates = "weekly";
        options = "--delete-older-than 14d";
    };

    # for Balena Etcher
    #              nixpkgs.config.permittedInsecurePackages = [
    #                "electron-19.1.9"
    #              ];

    programs.hyprland = {
        enable = true;
        #enableNvidiaPatches = true; # no longer needed
        xwayland.enable = true;
    };

    # hint electron apps to use wayland
    environment.sessionVariables = {
        NIXOS_OZONE_WL = "1";
    };

    # screen sharing
    services.dbus.enable = true;
    xdg.portal = {
        enable = true;
        wlr.enable = true;
        extraPortals = [
            pkgs.xdg-desktop-portal-gtk
        ];
    };

    nixpkgs.overlays = [

        # fix waybar not displaying hyprland workspaces
        (self: super: {
            waybar = super.waybar.overrideAttrs (oldAttrs: {
                mesonFlags = oldAttrs.mesonFlags ++ [ "-Dexperimental=true" ];
            });
        })

        # patch kernel in case of trouble
        # https://discourse.nixos.org/t/cant-update-nvidia-driver-on-stable-branch/39246/17?u=dob
        #(self: super: (
        #    let patched_pkgs = import inputs.nixpkgs_patched {
        #        inherit (self) system;
        #        config.allowUnfree = true;
        #    };
        #    in {
        #        linuxPackages = patched_pkgs.linuxPackages;
        #    }
        #))


    ];

    # if kernel patching is to be done, the next line is needed
    #boot.kernelPackages = pkgs.linuxPackages;


    fonts.packages = with pkgs; [
        nerdfonts
        #meslo-lgs-nf
    ];

    # Load nvidia driver for Xorg and Wayland
    services.xserver.videoDrivers = ["nvidia"];
    #services.xserver.videoDrivers = ["nvidia-dkms"];

    hardware.nvidia = {

        # Modesetting is required.
        modesetting.enable = true;

        powerManagement = {
                # Nvidia power management. Experimental, and can cause sleep/suspend to fail.
                enable = true; # must be false if prime's sync mode is used

                # Fine-grained power management. Turns off GPU when not in use.
                # Experimental and only works on modern Nvidia GPUs (Turing or newer).
                finegrained = true;
        };

        # Use the NVidia open source kernel module (not to be confused with the
        # independent third-party "nouveau" open source driver).
        # Support is limited to the Turing and later architectures. Full list of
        # supported GPUs is at:
        # https://github.com/NVIDIA/open-gpu-kernel-modules#compatible-gpus
        # Only available from driver 515.43.04+
        # Currently alpha-quality/buggy, so false is currently the recommended setting.
        open = false;

        # Optionally, you may need to select the appropriate driver version for your specific GPU.
        package = config.boot.kernelPackages.nvidiaPackages.stable;

        prime = {
            # Optimus PRIME Option A: Offload Mode
		    offload = {
		    	enable = true;
	    		enableOffloadCmd = true;
    		};

            # Optimus PRIME Option B: Sync Mode
            #sync.enable = true;

            # lshw -c display
            intelBusId = "PCI:0:2:0";
            nvidiaBusId = "PCI:2:0:0";
        };

        # Enable the Nvidia settings menu,
	    # accessible via `nvidia-settings`.
        nvidiaSettings = true;

    };

    hardware.opengl = {
        #extraPackages = with pkgs; [nvidia-vaapi-driver intel-media-driver];
        #extraPackages32 = with pkgs.pkgsi686Linux; [nvidia-vaapi-driver intel-media-driver];

        enable = true;
        driSupport = true;
        driSupport32Bit = true;
    };

    # not needed
    #programs.gamescope = {
    #    enable = true;
    #    capSysNice = true;
    #    args = [
    #        "--rt"
    #        # nvidia
    #        # lspci -nnk | grep NVIDIA
    #        "--prefer-vk-device 10de:134d"
    #        # intel
    #        # lspci -nnk | grep VGA
    #        # "--prefer-vk-device 8086:591b"
    #    ];
    #    env = {
    #        # direct gpu
    #        LIBVA_DRIVER_NAME = "nvidia";
    #
    #        # offload
    #        # __NV_PRIME_RENDER_OFFLOAD = "1";
    #        # __VK_LAYER_NV_optimus = "NVIDIA_only";
    #
    #        __GLX_VENDOR_LIBRARY_NAME = "nvidia";
    #    };
    #};


  #
  # Home Manager
  #

  home-manager.users.poq = {
    home.stateVersion = "23.11";
    home.file = {
      ".local/bin" = {
        source = ./sources/scripts;
        recursive = true;
      };
    };
  };



}
