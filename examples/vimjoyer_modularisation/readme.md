- https://www.youtube.com/watch?v=vYc6IzKvAJQ
- https://github.com/vimjoyer/modularize-video


in `flake.nix`, in `modules = []`, add:
```
# include (own) system module
./modules/system
```

then, in `configuration.nix`, add:

```
nvidia.enable = true;
```
