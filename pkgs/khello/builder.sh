source $stdenv/setup

mkdir -p $out/bin

cat >$out/bin/khello <<EOF
#!$SHELL
echo "Hello, World!"
EOF

chmod +x $out/bin/khello
