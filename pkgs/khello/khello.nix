{ stdenv }:

stdenv.mkDerivation {
    name = "khello";
    builder = ./builder.sh;
}
