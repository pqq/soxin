#!/usr/bin/env bash

# - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
# what:        download dotfiles
# author:      fk
# started:     feb 2024
#
# nifty:        https://github.com/dylanaraps/pure-bash-bible
#
# requires:    n/a
# - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
echo "<< 10_download_dotfiles.sh >>"

base_url=https://gitlab.com/pqq/soxin/-/raw/main
dots_url=${base_url}/dots
clone_url=https://gitlab.com/pqq/soxin.git
targz_url=https://gitlab.com/pqq/soxin/-/archive/main/soxin-main.tar.gz
clone_trg=/tmp/soxin
dots_trg=${clone_trg}/soxin-main/dots
pkgs_trg=${clone_trg}/soxin-main/pkgs
pkgs_dir=/home/.pkgs

if [ "$EUID" -ne 0 ]
  then echo "error: run as 'root'"
  exit
fi

this_file_name=`basename "$0"`
if [ $# -lt 1 ]; then
    echo "usage: '$this_file_name {all | ...}'"
    exit 1
fi

operation=$1

# ~ ~ ~ ~ ~ ~ ~ ~ ~ ~

# helper function
download_repo() {

   # make clone target directory, if it doesn't exist
    mkdir -p ${clone_trg}

    # instead of doing a git clone (like `git clone ${clone_url} ${clone_trg}`),
    # where i would have emptied the target dir each time, i download the
    # source as a tar.gz.
    wget -q ${targz_url} -O ${clone_trg}/soxin.tar.gz
    tar -xzf ${clone_trg}/soxin.tar.gz -C ${clone_trg}
    rm ${clone_trg}/soxin.tar.gz

}


# download all dots file from repo
if [ ${operation} == "dots-all" ] ; then
    echo "***** ${operation} *****"
    download_repo
    rm -rf /etc/nixos/*
    mv ${dots_trg}/* /etc/nixos/

# only download flake.nix
# (not being used, but still kept in case it will be needed in the future)
elif [ ${operation} == "flake.nix" ] ; then
    echo "***** ${operation} *****"
    wget -q ${dots_url}/${operation} -O /etc/nixos/${operation}

# download all packages from repo
elif [ ${operation} == "pkgs-all" ] ; then
    echo "***** ${operation} *****"
    download_repo
    rm -rf ${pkgs_dir}/*
    mkdir -p ${pkgs_dir}
    mv ${pkgs_trg}/* ${pkgs_dir}/

fi
