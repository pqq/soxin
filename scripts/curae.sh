#!/usr/bin/env bash

# - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
# what:        curae is latin an means to take care. this script takes care
#              of the system (adjusted for nixos).
# author:      fk
# started:     feb 2024
#
# nifty:        https://github.com/dylanaraps/pure-bash-bible
#
# requires:    tree
# - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -

#############################################################################
#
#   WARNING: EXAMINE THE CONTENT BEFORE EXECUTION. RUN AT YOUR OWN RISK.
#
#############################################################################
echo "<< curae.sh >>"

base_url=https://gitlab.com/pqq/soxin/-/raw/main
scripts_url=${base_url}/scripts
home_dir=/home/.curae
this_file=curae.sh
pkgs_dir=/home/.pkgs


if [ "$EUID" -ne 0 ]
  then echo "error: run as 'root'"
  exit
fi

this_file_name=`basename "$0"`
if [ $# -lt 1 ]; then
    echo "usage: '$this_file_name {pkg-build | u / upd-dots | upd-pkgs | upd-self | ur / upd-rebuild | urh / upd-rebuildh [host] | r / rebuild | rh / rebuildh [host]}'"
    exit 1
fi

operation=$1
extra_parameter=$2

shopt -s extglob
case $operation in
    !(pkg-build|u|upd-dots|upd-pkgs|upd-self|ur|urh|upd-rebuild|upd-rebuildh|r|rh|rebuild|rebuildh))
        echo "error: unknown operation '$operation'"
        exit
        ;;
esac
shopt -u extglob

# helper function
execute_script() {
    file=$1
    parameter=$2
    echo "execute: ${file} ${parameter}"
    wget -q ${scripts_url}/${file} -O /tmp/${file}
    chmod 755 /tmp/*.sh
    /tmp/${file} ${parameter}
}

# ~ ~ ~ ~ ~ ~ ~ ~ ~ ~

# build package
shopt -s extglob
if [ $operation == "pkg-build" ] ; then

    if [ -z "${extra_parameter}" ]; then
        echo "error: missing package name"
        exit 1
    fi

    package_dir=${pkgs_dir}/${extra_parameter}

    if ! [ -d "${package_dir}" ] ; then
      echo "invalid path: '${package_dir}'"
      exit 1
    fi

    echo "building package with path: '${package_dir}'"
    cd ${package_dir}
    nix-build -A ${extra_parameter}
    cd -

fi
shopt -u extglob

# update cfg/dot files
shopt -s extglob
if [ $operation == "u" ] || [ $operation == "upd-dots" ] ; then

    execute_script 10_download_files.sh "dots-all"

    tree -D /etc/nixos

fi
shopt -u extglob

# update packages
shopt -s extglob
if [ $operation == "upd-pkgs" ] ; then

    execute_script 10_download_files.sh "pkgs-all"

fi
shopt -u extglob

# self update, by re-downloading and overwriting itself
shopt -s extglob
if [ $operation == "upd-self" ] ; then

    wget -v ${base_url}/scripts/${this_file} -O ${home_dir}/${this_file}.tmp
    cp ${home_dir}/${this_file}.tmp ${home_dir}/${this_file}
    rm ${home_dir}/${this_file}.tmp
    chmod 755 ${home_dir}/*.sh

fi
shopt -u extglob

# update and rebuild (all) with hostname
shopt -s extglob
if [ $operation == "urh" ] || [ $operation == "upd-rebuildh" ] ; then

    if [ -z "${extra_parameter}" ]; then
        echo "error: missing host name"
        exit 1
    fi

    ${home_dir}/${this_file} upd-dots
    ${home_dir}/${this_file} rebuild ${extra_parameter}

fi
shopt -u extglob

# update and rebuild (all) without hostname
shopt -s extglob
if [ $operation == "ur" ] || [ $operation == "upd-rebuild" ] ; then

    ${home_dir}/${this_file} upd-dots
    ${home_dir}/${this_file} rebuild ${extra_parameter}

fi
shopt -u extglob

# rebuild system with hostname
shopt -s extglob
if [ $operation == "rh" ] || [ $operation == "rebuildh" ] ; then

    if [ -z "${extra_parameter}" ]; then
        echo "error: missing host name"
        exit 1
    fi

    nixos-rebuild switch -v --show-trace --flake /etc/nixos#${extra_parameter}

fi
shopt -u extglob

# rebuild system without hostname
shopt -s extglob
if [ $operation == "r" ] || [ $operation == "rebuild" ] ; then

    nixos-rebuild switch -v --show-trace

fi
shopt -u extglob
