#!/usr/bin/env bash

# - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
# what:        creates .curae home dir and downloads curae.sh
# author:      fk
# started:     feb 2024
#
# requires:    n/a
#
# how to download (as root):
# WARNING: ALWAYS CHECK THE SCRIPT BEFORE RUNNING IT!
#
#   cd /tmp
#   wget tinyurl.com/fk-genesis     # same as `wget https://gitlab.com/pqq/soxin/-/raw/main/scripts/genesis.sh`
#   cat fk-genesis                  # verify the downloaded script
#   chmod 755 fk-genesis            # make the script executable
#   ./fk-genesis                    # run the script
# - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
echo "<< genesis.sh >>"

base_url=https://gitlab.com/pqq/soxin/-/raw/main
home_dir=/home/.curae

mkdir -p ${home_dir}
cd ${home_dir}
wget -q ${base_url}/scripts/curae.sh -O ${home_dir}/curae.sh
chmod 755 curae.sh

# copy fk-genesis (genesis.sh) into the new home dir, for potential later use
if [ -f "/tmp/fk-genesis" ]; then
    cp /tmp/fk-genesis ${home_dir}/genesis.sh
fi

echo "a fresh copy of curae.sh is now located at '${home_dir}/curae.sh'"
